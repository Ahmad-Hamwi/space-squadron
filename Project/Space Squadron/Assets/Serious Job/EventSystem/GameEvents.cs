﻿using System;
using UnityEngine;

/* 
 * This Class is a Singleton class which has all the game events
 * 
 * It has the events and the function that fires it after checking if it has subscibers.
 * 
 */

public class GameEvents
{
    public static GameEvents instance = new GameEvents();
    private GameEvents() { }

    public static GameEvents Instance()
    {
        return instance;
    }


    /* 
     * The Following are a set of events of the whole game :
     */

    //Events for Destroying an Object.
    public event Action<Vector3> onAstroidDestruction;
    public event Action<Vector3> onEnemyShipDestruction;
    public event Action<Vector3> onStationDestruction;

    public event Action onTargetDestruction;


    //Event for player Death.
    public event Action <string> onPlayerDeath;


    //Event when health changes (increases or decreases).
    public event Action onPlayerHealthChanged;

    public event Action<Target> onTargetHealthChanged;


    //Event when score changes (increases).. It takes in the addedScore attribute
    public event Action<int> onScoreChange;


    public event Action onStationSpawned;

    public event Action onSpawnPortal;

    public event Action onLevelWin;
    public event Action onLevelLose;

    public event Action <String> onTargetShooting;

    public event Action onControlsSelected;

    public event Action<int, string> onScoreChangeForPlayer;

    public event Action onReloadCurrentScene;

    /*
     * 
     * The following are functions for fireing events if they have subscribers
     * 
     */
    public void AstroidDestruction(Vector3 position)
    {
        onAstroidDestruction?.Invoke(position);
    }

    public void EnemyShipDestruction(Vector3 position)
    {
        onEnemyShipDestruction?.Invoke(position);
    }

    public void StationDestruction(Vector3 position)
    {
        onStationDestruction?.Invoke(position);
    }

    public void PlayerDeath(string name)
    {
        onPlayerDeath?.Invoke(name);
    }

    public void PlayerHealthChanged()
    {
        onPlayerHealthChanged?.Invoke();
    }

    public void ScoreChange(int addedScore)
    {
        onScoreChange?.Invoke(addedScore);
    }

    public void TargetDestruction()
    {
        onTargetDestruction?.Invoke();
    }

    public void TargetHealthChanged(Target target)
    {
        onTargetHealthChanged?.Invoke(target);
    }

    public void StationSpawned()
    {
        onStationSpawned?.Invoke();
    }

    public void SpawnPortal()
    {
        onSpawnPortal?.Invoke();
    }

    public void LevelWin()
    {
        onLevelWin?.Invoke();
    }
    public void LevelLose()
    {
        onLevelLose?.Invoke();
    }

    public void TargetShooting(String playerName)
    {
        onTargetShooting?.Invoke(playerName);
    }

    public void ScoreChangeForPlayer(int score, string name)
    {
        onScoreChangeForPlayer?.Invoke(score, name);
    }

    public Action getOnControlsSelected()
    {
        return onControlsSelected;
    }

    public void ReloadCurrentScene()
    {
        onReloadCurrentScene?.Invoke();
    }
}
