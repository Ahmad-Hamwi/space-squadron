﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    //Score Text UI element.
    public Text scoreText;

    //A player Current Score.
    public int currentScore;

    //High Score of all time    
    public int highScore;

    public int kills = 0;

    // Start is called before the first frame update
    void Start()
    {
        //Get The Score Text UI Element.
        //scoreText = GameObject.Find("ScoreText").GetComponent<Text>();

        //When starting the game start with a zero score.
        currentScore = 0;

        //Subscribe to OnScoreChange Event.
        GameEvents.instance.onScoreChange += Instance_onScoreChange;
        GameEvents.instance.onScoreChangeForPlayer += Instance_onScoreChangeForPlayer;
        GameEvents.instance.onEnemyShipDestruction += Instance_OnKill;
        GameEvents.instance.onStationDestruction += Instance_OnKill;

        //Get HighScore From Repo
        //hightScore = ....

    }

    private void Instance_onScoreChangeForPlayer(int addedScore, string playerName)
    {
        if(playerName == "Player")
        {
            scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        }else if(playerName == "Player1")
        {
            scoreText = GameObject.Find("ScoreText1").GetComponent<Text>();
        }else if(playerName == "Player2")
        {
            scoreText = GameObject.Find("ScoreText2").GetComponent<Text>();
        }

        //Add to the current score
        currentScore += addedScore;
        //format the current score to be a string and update the value of the UI Score Text.
        if (scoreText != null)
        {
            scoreText.text = currentScore.ToString("D9");
        }
    }

    private void Instance_onScoreChange(int addedScore)
    {
        //Add to the current score
        currentScore += addedScore;
        //format the current score to be a string and update the value of the UI Score Text.
        if(scoreText != null) { 
        scoreText.text = currentScore.ToString("D9");
        }
    }

    private void Instance_OnKill(Vector3 position)
    {
        kills++;
        Debug.Log("kills: " + kills);
    }
}
