﻿using System.Security.Cryptography;
using Space_Squadron.Assets.Data.Entities;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHandler : MonoBehaviour
{
    [SerializeField]
    public HealthBar healthBar { get; set; }


    [SerializeField]
    private float health;

    private HealthSystem healthSystem;

    // Start is called before the first frame update
    void Start()
    {
        setUp();

        healthSystem = new HealthSystem(health);
        healthBar = new PlayerHealthBar(healthSystem, this.name);

        healthSystem.onDeathEvent += Instance_onPlayerDeath;
    }

    private void setUp()
    {
        SpaceShipEntity spaceShipEntity = DataManager.instance.GetPlayerSpaceShip();
        this.health = spaceShipEntity.hp;
    }

    private void Instance_onPlayerDeath()
    {
        Debug.Log("Playeer Died!");
        //GameEvents.instance.LevelLose();

        GameEvents.instance.PlayerDeath(this.name);

        GameObject.Destroy(gameObject); 

        //GameObject.Find("CoinsManager").GetComponent<CoinsManager>().calculateCoins();
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }    

    private void OnCollisionEnter(Collision collision)
    {
        Target target = collision.gameObject.GetComponent<Target>();
        if(target != null)
        {
            healthSystem.TakeDamage(20f * GetComponent<Engine>().speed / 100);
            target.Damage(20f * GetComponent<Engine>().speed / 100);
        }
    }

    public void Damage(float dmg)
    {
        this.healthSystem.TakeDamage(dmg);
    }

    public void Heal(float extraHealth)
    {
        this.healthSystem.Heal(extraHealth);
    }
        

}
