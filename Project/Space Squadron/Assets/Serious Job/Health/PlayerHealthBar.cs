﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBar : HealthBar
{

    public PlayerHealthBar(HealthSystem healthSystem) : base(healthSystem) {
        this.healthBarObject = GameObject.Find("PlayerCanvas").GetComponentInChildren<Slider>();
    }

    public PlayerHealthBar(HealthSystem healthSystem, string playerName) : base(healthSystem)
    {
        if (playerName == "Player") {
            this.healthBarObject = GameObject.Find("HealthBarPlayer").GetComponentInChildren<Slider>();
        }
        else if (playerName == "Player1")
        {
            this.healthBarObject = GameObject.Find("HealthBarPlayer1").GetComponentInChildren<Slider>();
        }
        else if (playerName == "Player2")
        {
            this.healthBarObject = GameObject.Find("HealthBarPlayer2").GetComponentInChildren   <Slider>();
        }

    }

    public override void AttachEventHealthChange()
    {
        healthSystem.onHealthChanged += HealthSystem_onHealthChanged;
    }

    private void HealthSystem_onHealthChanged()
    {
        healthBarObject.value = healthSystem.GetHealthPercentage();
        healthBarObject.gameObject.transform.Find("Fill Area").Find("Fill").GetComponent<Image>().color =
                Color.Lerp(MinHealthColor, MaxHealthColor, this.healthSystem.GetHealthPercentage());
    }

}
