﻿using System;
using System.Collections;
using System.Collections.Generic;
using Space_Squadron.Assets.Data.Entities;
using Space_Squadron.Assets.Data.Entities.GameData;
using Space_Squadron.Assets.Data.Repositories;
using UnityEngine;

public class DataManager : MonoBehaviour
{

    public static DataManager instance;

    public InGameRepository inGameRepository;



    private SpaceShipEntity playerSpaceShip;
    private SceneEntity currentSceneData;


    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        inGameRepository = InGameRepositoryImpl.GetInstance();

        setup();
    }

    private void setup()
    {
        playerSpaceShip = inGameRepository.GetCurrentPlayerSpaceShip();
        currentSceneData = inGameRepository.GetCurrentScene();

        //Debug.Log($"in game repo is getting {inGameRepository.GetPlayerControlType()} for the first player");
        //Debug.Log($"in game repo is getting {inGameRepository.GetPlayerControlType()} for the second player");
    }

    public SpaceShipEntity GetPlayerSpaceShip()
    {
        return this.playerSpaceShip;
    }

    public SpaceShipEntity GetSecondPlayerSpaceShip()
    {
        return inGameRepository.GetPlayer2SpaceShip();
    }

    public SceneEntity GetSceneEntity()
    {
        return this.currentSceneData;
    }


    public int GetPlayerControlType()
    {
        
        return inGameRepository.GetPlayerControlType();
    }

    public int GetSecondPlayerControlType()
    {
        return inGameRepository.GetPlayer2ControlType();
    }
    
}
