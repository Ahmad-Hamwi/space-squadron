﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class PlayerAudioManager : MonoBehaviour
{
    private AudioSource engineAudioSource;
    private AudioSource turboAudioSource;
    private AudioSource musicAudioSource;
    public float gameSoundsVolume;  // read this value from settings
    public float gameMusicVolume;   // read this value from settings
    private Engine engine;
    string[] musicTracks;
    [SerializeField] private AudioMixerGroup master;

    private AudioSource SetUpAudioSource(string path)
    {
        AudioSource audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = Resources.Load<AudioClip>(path);
        audioSource.loop = true;
        audioSource.outputAudioMixerGroup = master;
        audioSource.Play();
        return audioSource;
    }
    
    void Awake()
    {
        musicTracks = new string[] {"open", "path"};
        engineAudioSource = SetUpAudioSource("FlightWind");
        turboAudioSource = SetUpAudioSource("JetEngine");
        musicAudioSource = SetUpAudioSource(musicTracks[SceneManager.GetActiveScene().buildIndex - 1]);
    }

    void Start()
    {
        gameSoundsVolume = 0.5f;
        gameMusicVolume = 0.5f;
        engine = gameObject.GetComponent<Engine>();
    }

    void Update()
    {
        engineAudioSource.volume = gameSoundsVolume * (0.25f + 0.75f * engine.translationSpeedManager.speedPercentage);
        turboAudioSource.volume = gameSoundsVolume * engine.turpoSpeedManager.speedPercentage *
                                                        engine.translationSpeedManager.speedPercentage;
    }
}
