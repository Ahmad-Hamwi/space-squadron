﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyTransform
{
    public float time;
    public Vector3 position;
    public Vector3 forward;
    public Vector3 right;
    public Vector3 up;

    public MyTransform(float time, Vector3 position, Vector3 forward, Vector3 right, Vector3 up)
    {
        this.time = time;
        this.position = position;
        this.forward = forward;
        this.right = right;
        this.up = up;
    }

    public MyTransform(float time, Transform transform) : this(time, transform.position, transform.forward, transform.right, transform.up) { }
}
