﻿using System;
using UnityEngine;

public class OpenWorldPlayerController : MonoBehaviour
{
    private int oldControllerType;
    [Range(1, 2)] public int controllerType = 1;
    private Engine engine;
    private InputActionsManager inputActionsManager;
    [SerializeField] private GameObject pauseMenuManager;

    private void SetUpControllerType()
    {
        if(gameObject == GameObject.FindGameObjectsWithTag("Player")[0])
        {
            controllerType = 1;
        }

        else
        {
            controllerType = 2;
        }
    }
    private void SetUpInputDependence()
    {
        oldControllerType = controllerType;
        
        switch(controllerType)
        {
            case 1: inputActionsManager.inputDependence = InputDependence.OpenWorldKeyboard[0]; break;
            case 2: inputActionsManager.inputDependence = InputDependence.OpenWorldJoystick[0]; break;
            default: throw new Exception("invalid controller type");
        }
    }
    
    private void SetUpInputActionsManager()
    {
        inputActionsManager = gameObject.AddComponent<InputActionsManager>();
        inputActionsManager.inputActions = new InputActions();

        SetUpControllerType();
        SetUpInputDependence();
    }

    private void SetUpEngine()
    {
        // TODO : Read values from file.
        engine = gameObject.AddComponent<Engine>();
        engine.SetDistanceFromRotationAxis(50f);
        engine.SetTranlationSpeedManger(0f, 75f);
        engine.SetRotationSpeedManger(0f, 50f);
        engine.SetTurboSpeedManger(0f, 30f);
    }

    void Awake()
    {
        SetUpEngine();
        SetUpInputActionsManager();
        pauseMenuManager = GameObject.Find("PauseMenuManager");
    }

    void Start()
    {
        inputActionsManager.inputActions.onTranslationsClick += PerformTranslation;
        inputActionsManager.inputActions.onRotationsClick += PerformRotation;
        inputActionsManager.inputActions.onEnableTurboClick += engine.AcceptTurbo;
        inputActionsManager.inputActions.onOpenMenuClick += pauseMenuManager.GetComponent<PauseMenuManager>().PauseUnPauseGame;

        GameEvents.instance.onControlsSelected += SetUpInputDependence;
    }

    void FixedUpdate()
    {
        if(oldControllerType != controllerType)
            SetUpInputDependence();
    }

    private void PerformTranslation(Vector3 translationVector)
    {
        translationVector = translationVector.x * transform.right + 
                            translationVector.y * transform.up + 
                            translationVector.z * transform.forward;
        new TranslateMove(engine, translationVector).PerformMove();
    }

    private void PerformRotation(Vector3 rotationVector)
    {
        new RotateMove(engine, rotationVector.x * transform.right).PerformMove();
        new RotateMove(engine, rotationVector.y * transform.up).PerformMove();
        new RotateMove(engine, rotationVector.z * transform.forward).PerformMove();
    }
}
