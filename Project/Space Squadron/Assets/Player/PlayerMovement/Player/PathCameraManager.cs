﻿using System.Collections.Generic;
using UnityEngine;

public class PathCameraManager : MonoBehaviour
{
    private Vector3 distance;

    void Start()
    {
        distance = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<Transform>().position 
                    + new Vector3(0f, 25f, -100f);
        distance.x = 0;
    }

    void FixedUpdate()
    {
        if (GameObject.FindGameObjectsWithTag("Player").Length > 0)
        {
            Transform currentPersonTransform = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<Transform>();
            transform.position = new Vector3(0f, 0f, currentPersonTransform.position.z) + distance;
            transform.rotation = Quaternion.LookRotation(Vector3.forward - 0.01f * Vector3.up, Vector3.up);
        }
    }
}
