﻿using System;
using UnityEngine;

public class PathPlayerController : MonoBehaviour
{
    private int oldControllerType;
    [Range(1, 4)] public int controllerType;
    private Engine engine;
    private InputActionsManager inputActionsManager;
    [SerializeField] private GameObject pauseMenuManager;

    private void SetUpControllerType()
    {
        if(gameObject == GameObject.FindGameObjectsWithTag("Player")[0])
        {
            controllerType = 1;
        }

        else
        {
            controllerType = 2;
        }
    }
    
    private void SetUpInputDependence()
    {
        oldControllerType = controllerType;
        
        switch(controllerType)
        {
            case 1: inputActionsManager.inputDependence = InputDependence.PathKeyboard[0]; break;
            case 2: inputActionsManager.inputDependence = InputDependence.PathKeyboard[1]; break;
            case 3: inputActionsManager.inputDependence = InputDependence.PathJoystick[0]; break;
            case 4: inputActionsManager.inputDependence = InputDependence.PathJoystick[1]; break;
            default: throw new Exception("invalid controller type");
        }
    }
    
    private void SetUpInputActionsManager()
    {
        inputActionsManager = gameObject.AddComponent<InputActionsManager>();
        inputActionsManager.inputActions = new InputActions();
        
        SetUpControllerType();
        SetUpInputDependence();
    }

    private void SetUpEngine()
    {
        // TODO : Read values from file.
        engine = gameObject.AddComponent<Engine>();
        engine.SetDistanceFromRotationAxis(50f);
        engine.SetTranlationSpeedManger(0f, 75f);
        engine.SetRotationSpeedManger(0f, 50f);
        engine.SetTurboSpeedManger(0f, 30f);
    }

    void Awake()
    {
        SetUpEngine();
        SetUpInputActionsManager();
    }

    void Start()
    {
        inputActionsManager.inputActions.onTranslationsClick += PerformTranslationIgnoreZ;
        // inputActionsManager.inputActions.onOpenMenuClick += pauseMenuManager.GetComponent<PauseMenuManager>().PauseUnPauseGame;

        // GameEvents.instance.onControlsSelected += SetUpInputDependence;
    }

    void FixedUpdate()
    {
        PerformTranslation(Vector3.forward);        
        
        if(gameObject != GameObject.FindGameObjectsWithTag("Player")[0])
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 
                GameObject.FindGameObjectsWithTag("Player")[0].transform.position.z);
        }

        if(oldControllerType != controllerType)
            SetUpInputDependence();
    }

    private void PerformTranslationIgnoreZ(Vector3 translationVector)
    {
        translationVector.z = 0;
        PerformTranslation(translationVector);
    }

    private void PerformTranslation(Vector3 translationVector)
    {
        translationVector = translationVector.x * transform.right + 
                            translationVector.y * transform.up + 
                            translationVector.z * transform.forward;
        new TranslateMove(engine, translationVector).PerformMove();
    }

    private void PerformRotation(Vector3 rotationVector)
    {
        new RotateMove(engine, rotationVector.x * transform.right).PerformMove();
        new RotateMove(engine, rotationVector.y * transform.up).PerformMove();
        new RotateMove(engine, rotationVector.z * transform.forward).PerformMove();
    }
}
