﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputDependence
{
    public TranslationKeys translationKeys;
    public RotationKeys rotationKeys;
    public string primaryShootingKey, altPrimaryShootingKey;
    public string secondaryShootingKey, altSecondaryShootingKey;
    public string usePrimarySpecialItemKey, useSecondarySpecialItemKey;
    public string enableTurbo;
    public string openMenu;


    public InputDependence() 
    { 
        translationKeys = new TranslationKeys();
        rotationKeys = new RotationKeys();
    }

    public InputDependence(TranslationKeys translationKeys, RotationKeys rotationKeys, 
                            string primaryShootingKey, string altPrimaryShootingKey, 
                            string secondaryShootingKey, string altSecondaryShootingKey, 
                            string usePrimarySpecialItemKey, string useSecondarySpecialItemKey, 
                            string enableTurbo, string openMenu)
    {
        this.translationKeys = translationKeys;
        this.rotationKeys = rotationKeys;
        this.primaryShootingKey = primaryShootingKey;
        this.altPrimaryShootingKey = altPrimaryShootingKey;
        this.secondaryShootingKey = secondaryShootingKey;
        this.altSecondaryShootingKey = altSecondaryShootingKey;
        this.usePrimarySpecialItemKey = usePrimarySpecialItemKey;
        this.useSecondarySpecialItemKey = useSecondarySpecialItemKey;
        this.enableTurbo = enableTurbo;
        this.openMenu = openMenu;
    }

    public bool IsTranslationKeysPressed()
    {
        return translationKeys.IsAnyKeyPressed();
    }
    public bool IsRotationKeysPressed()
    {
        return rotationKeys.IsAnyKeyPressed();
    }
    public bool IsPrimaryShootingKeyPressed()
    {
        return Input.GetKey(primaryShootingKey) || Input.GetKey(altPrimaryShootingKey);
    }
    public bool IsSecondaryShootingKeyPressed()
    {
        return Input.GetKey(secondaryShootingKey) || Input.GetKey(altSecondaryShootingKey);
    }
    public bool IsUsePrimarySpecialItemKeyPressed()
    {
        return Input.GetKey(usePrimarySpecialItemKey);
    }
    public bool IsUseSecondarySpecialItemKeyPressed()
    {
        return Input.GetKey(useSecondarySpecialItemKey);
    }
    public bool IsEnableTurboKeyPressed()
    {
        return Input.GetKey(enableTurbo);
    }
    public bool IsOpenMenuKeyPressed()
    {
        return Input.GetKeyDown(openMenu);
    }

    public static InputDependence[] OpenWorldKeyboard = 
    {
        new InputDependence(new TranslationKeys("w", "s", "a", "d", "q", "z", "NONE", "NONE", "NONE"), 
        new RotationKeys("down", "up", "right", "left", ",", ".", "Mouse Y", "Mouse X", "Mouse ScrollWheel"),
        "space", "mouse 0", "v", "mouse 1", "~", "~", "left shift", "escape"), 

        new InputDependence(new TranslationKeys("w", "s", "a", "d", "q", "z", "NONE", "NONE", "NONE"), 
        new RotationKeys("down", "up", "right", "left", ",", ".", "Mouse Y", "Mouse X", "Mouse ScrollWheel"),
        "space", "mouse 0", "v", "mouse 1", "~", "~", "left shift", "escape")
    };

    public static InputDependence[] PathKeyboard = 
    {
        new InputDependence(new TranslationKeys("~", "~", "a", "d", "w", "s", "NONE", "NONE", "NONE"), 
        new RotationKeys("down", "up", "right", "left", ",", ".", "Mouse Y", "Mouse X", "Mouse ScrollWheel"),
        "space", "mouse 0", "v", "mouse 1", "~", "~", "~", "escape"), 

        new InputDependence(new TranslationKeys("~", "~", "left", "right", "up", "down", "NONE", "NONE", "NONE"), 
        new RotationKeys("down", "up", "right", "left", ",", ".", "Mouse Y", "Mouse X", "Mouse ScrollWheel"),
        "[0]", "[1]", "~", "~", "~", "~", "~", "~")
    };

    public static InputDependence[] OpenWorldJoystick = 
    {
        new InputDependence(new TranslationKeys("joystick 1 button 2", "~", "~", "~", "~", "~", "NONE", "NONE", "NONE"), 
        new RotationKeys("~", "~", "~", "~", "joystick 1 button 4", "joystick 1 button 5", "Joystick 1 Y", "Joystick 1 X", "NONE"),
        "joystick 1 button 3", "~", "joystick 1 button 6", "~", "~", "~", "joystick 1 button 7", "joystick 1 button 9"), 

        new InputDependence(new TranslationKeys("joystick 2 button 2", "~", "~", "~", "~", "~", "NONE", "NONE", "NONE"), 
        new RotationKeys("~", "~", "~", "~", "joystick 2 button 4", "joystick 2 button 5", "Joystick 2 Y", "Joystick 2 X", "NONE"),
        "joystick 2 button 3", "~", "joystick 2 button 6", "~", "~", "~", "joystick 2 button 7", "joystick 2 button 9")
    };

    public static InputDependence[] PathJoystick = 
    {
        new InputDependence(new TranslationKeys("~", "~", "~", "~", "~", "~", "Joystick 1 X", "Joystick 1 Y", "NONE"), 
        new RotationKeys("~", "~", "~", "~", "~", "~", "NONE", "NONE", "NONE"),
        "joystick 1 button 3", "~", "joystick 1 button 6", "~", "~", "~", "~", "joystick 1 button 9"), 

        new InputDependence(new TranslationKeys("~", "~", "~", "~", "~", "~", "Joystick 2 X", "Joystick 2 Y", "NONE"), 
        new RotationKeys("~", "~", "~", "~", "~", "~", "NONE", "NONE", "NONE"),
        "joystick 2 button 3", "~", "joystick 2 button 6", "~", "~", "~", "~", "joystick 2 button 9")
    };
}
