﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslationKeys
{
    public string forwardKey;
    public string backwardKey;
    public string leftwardKey;
    public string rightwardKey;
    public string upwardKey;
    public string downwardKey;
    public string translateXAxis;
    public string translateYAxis;
    public string translateZAxis;

    public TranslationKeys() { }
    public TranslationKeys(string forwardKey, string backwardKey, string leftwardKey, string rightwardKey, string upwardKey, string downwardKey, string translateXAxis, string translateYAxis, string translateZAxis)
    {
        this.forwardKey = forwardKey;
        this.backwardKey = backwardKey;
        this.leftwardKey = leftwardKey;
        this.rightwardKey = rightwardKey;
        this.upwardKey = upwardKey;
        this.downwardKey = downwardKey;
        this.translateXAxis = translateXAxis;
        this.translateYAxis = translateYAxis;
        this.translateZAxis = translateZAxis;
    }

    private bool IsAnyKeyPressed1()
    {
        return Input.GetKey(rightwardKey) || Input.GetKey(leftwardKey) || 
                Input.GetKey(upwardKey) || Input.GetKey(downwardKey) || 
                Input.GetKey(forwardKey) || Input.GetKey(backwardKey);
    }

    private bool IsAnyKeyPressed2()
    {
        return Mathf.Abs(Input.GetAxis(translateXAxis)) > 0.1f ||
                Mathf.Abs(Input.GetAxis(translateYAxis)) > 0.1f || 
                Mathf.Abs(Input.GetAxis(translateZAxis)) > 0.1f;
    }

    public bool IsAnyKeyPressed()
    {
        return IsAnyKeyPressed1() || IsAnyKeyPressed2();
    }

    public Vector3 GetTranslationVector()
    {
        return new Vector3((Input.GetKey(rightwardKey) ? +1 : 0) - (Input.GetKey(leftwardKey) ? +1 : 0),
                            (Input.GetKey(upwardKey) ? +1 : 0) - (Input.GetKey(downwardKey) ? +1 : 0), 
                            (Input.GetKey(forwardKey) ? +1 : 0) - (Input.GetKey(backwardKey) ? +1 : 0))
                            + new Vector3(Input.GetAxis(translateXAxis), Input.GetAxis(translateYAxis), Input.GetAxis(translateZAxis));
    }    
}