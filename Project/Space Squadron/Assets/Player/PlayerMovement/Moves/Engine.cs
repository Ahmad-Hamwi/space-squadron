﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engine : MonoBehaviour
{
    public float distanceFromRotationAxis;
    public SpeedManager translationSpeedManager;
    public SpeedManager rotationSpeedManager;
    public SpeedManager turpoSpeedManager;

    public float translationSpeed 
    {
        get 
        {
            return translationSpeedManager.speed + turpoSpeedManager.speed;
        }
    }

    public float speed
    {
        get 
        {
            return translationSpeed + turpoSpeedManager.speed;
        }
    }

    void Awake()
    {
        translationSpeedManager = gameObject.AddComponent<SpeedManager>();
        rotationSpeedManager = gameObject.AddComponent<SpeedManager>();
        turpoSpeedManager = gameObject.AddComponent<SpeedManager>();

        /// you are not supposed to be here
        SetDistanceFromRotationAxis(0f);
        SetTranlationSpeedManger(0f, 75f);
        SetRotationSpeedManger(0f, 50f);
        SetTurboSpeedManger(0f, 30f);
    }

    public void AcceptTurbo()
    {
        turpoSpeedManager.AcceptAcceleration();
    }

    public void AcceptTranslation()
    {
        AcceptTranslation(transform.forward);
    }

    public void AcceptTranslation(Vector3 translationAxis)
    {
        // translationSpeedManager.maxSpeed = 75;  // why is this here
        translationSpeedManager.AcceptAcceleration(translationAxis.magnitude);
        gameObject.GetComponent<CharacterController>().Move(translationAxis * translationSpeed * Time.deltaTime);        
    }

    public void AcceptRotation(Vector3 rotationAxis)
    {
        // rotationSpeedManager.maxSpeed = 50f;    // and this
        rotationSpeedManager.AcceptAcceleration(rotationAxis.magnitude);
        Vector3 offsetVector = -Vector3.Dot(rotationAxis, transform.forward) * transform.right +
                            Mathf.Abs(Vector3.Dot(rotationAxis, transform.right)) * transform.up +
                            Mathf.Abs(Vector3.Dot(rotationAxis, transform.up)) * transform.forward;
        transform.RotateAround(transform.position + distanceFromRotationAxis * offsetVector, 
                                rotationAxis, rotationSpeedManager.speed * Time.deltaTime);
    }

    public void AcceptTurnning(Vector3 rotationAxis, Vector3 desiredForward)
    {
        if(Vector3.Angle(desiredForward, transform.forward) != 0)
        {
            float oldAngle = Vector3.Angle(desiredForward, transform.forward);
            AcceptRotation(rotationAxis);
            float newAngle = Vector3.Angle(desiredForward, transform.forward);
            if((newAngle < 180 && oldAngle > 180) || (newAngle > 180 && oldAngle < 180))
            {
                transform.forward = desiredForward.normalized;
            }
        }
    }

    public void SetDistanceFromRotationAxis(float distanceFromRotationAxis)
    {
        this.distanceFromRotationAxis = distanceFromRotationAxis;
    }

    public void SetTranlationSpeedManger(float newMinSpeed,  float newMaxSpeed)
    {
        translationSpeedManager.minSpeed = newMinSpeed;
        translationSpeedManager.maxSpeed = newMaxSpeed;
    }

    public void SetRotationSpeedManger(float newMinSpeed,  float newMaxSpeed)
    {
        rotationSpeedManager.minSpeed = newMinSpeed;
        rotationSpeedManager.maxSpeed = newMaxSpeed;
    }

    public void SetTurboSpeedManger(float newMinSpeed,  float newMaxSpeed)
    {
        turpoSpeedManager.minSpeed = newMinSpeed;
        turpoSpeedManager.maxSpeed = newMaxSpeed;
    }
}
