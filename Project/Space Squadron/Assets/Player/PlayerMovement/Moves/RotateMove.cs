﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateMove : BaseMoveUtil
{
    private Engine movingObjectEngine;
    private Vector3 rotationVector;

    public RotateMove(Engine movingObjectEngine, Vector3 rotationVector)
    {
        this.movingObjectEngine = movingObjectEngine;
        this.rotationVector = rotationVector;
    }

    public override void PerformMove()
    {
        movingObjectEngine.AcceptRotation(rotationVector);
    }
}
