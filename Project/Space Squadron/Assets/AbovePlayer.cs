﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbovePlayer : MonoBehaviour
{

    [SerializeField] private GameObject target;
    [SerializeField] private string targetName = "Player";
    [SerializeField] private RectTransform myIndicator;
    [SerializeField] private Vector3 offset = new Vector3(0, 10, 0);

    // Start is called before the first frame update
    void Start()
    {
        if(target == null)
        {
            target = GameObject.Find(targetName);
        }        
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            this.myIndicator.position = Camera.main.WorldToScreenPoint(
                target.transform.position + offset
                ); ;
        }
        else
        {
            this.gameObject.SetActive(false);
        }
    }
}
