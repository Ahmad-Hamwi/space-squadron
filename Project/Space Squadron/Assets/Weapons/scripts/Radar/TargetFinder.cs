using UnityEngine;
using System.Collections.Generic;
public class TargetFinder
{
    public static Transform FindNearestTarget(GameObject ship)
    {
        RoleType targetType = Role.GetTargetType(Role.GetRoleType(ship));

        GameObject[] targets = GameObject.FindGameObjectsWithTag(targetType.ToString());

        int closestIndex = -1;
        float dist = Mathf.Infinity;

        for(int i = 0;i<targets.Length;i++)
        {
            if(Vector3.Distance(targets[i].transform.position, ship.transform.position) < dist)
            {
                closestIndex = i;
                dist = Vector3.Distance(targets[i].transform.position, ship.transform.position);
            }
        }

        if(closestIndex < 0 || closestIndex > targets.Length - 1)
            return null;

        return targets[closestIndex].transform;
    }
    
    public static Transform GetTarget(GameObject ship, float range, float maxAngle)
    {
        RoleType[] targetTypes = new RoleType[3]
        {
            Role.GetTargetType(Role.GetRoleType(ship)),
            RoleType.SpaceStation,
            RoleType.SpaceObject
        };

        return GetTarget(ship, targetTypes, range, maxAngle);
    }

    public static Transform GetTarget(GameObject ship, RoleType[] targetTypes, float range, float maxAngle)
    {
        for(int i = 0;i<targetTypes.Length;i++)
        {
            GameObject[] targets = GameObject.FindGameObjectsWithTag(targetTypes[i].ToString());  
            int closestIndex = -1;
            float dist = Mathf.Infinity;

            for(int j = 0;j<targets.Length;j++)
            {
                if(Vector3.Distance(targets[j].transform.position, ship.transform.position) <= range)
                {
                    float angle = Vector3.Angle(targets[j].transform.position - ship.transform.position, ship.transform.forward);
                    if(angle > 180)
                    {
                        angle -= 360;
                        angle *= -1;
                    }

                    if(angle < dist && angle <= maxAngle)
                    {
                        closestIndex = j;
                        dist = angle;
                    }
                }
            }

            if(closestIndex < 0 || closestIndex > targets.Length - 1)
            {
                continue;
            }  
            return targets[closestIndex].transform;
        }

        return null;

    }

    public static List<GameObject> GetAllOfType(RoleType type, Transform center = null, float radius = Mathf.Infinity)
    {
        GameObject[] targets = GameObject.FindGameObjectsWithTag(type.ToString());
        List<GameObject> res = new List<GameObject>();

        for(int i = 0;i<targets.Length;i++)
        {
            if(center != null)
            {
                if(Vector3.Distance(center.position, targets[i].transform.position) <= radius)
                    res.Add(targets[i]);
            }
            else 
            {
                res.Add(targets[i]);
            }
        }

        return res;
    }
}