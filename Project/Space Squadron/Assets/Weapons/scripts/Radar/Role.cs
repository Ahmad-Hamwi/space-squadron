using UnityEngine;
using System;

public class Role
{
    
    public static RoleType GetTargetType(RoleType type)
    {
        switch(type)
        {
            case RoleType.Ally:
                return RoleType.Enemy;
            
            case RoleType.Enemy:
                return RoleType.Ally;
            
            default:
                return RoleType.Other;
        }
    }

    public static RoleType GetRoleType(GameObject gameObject)
    {
        if(gameObject != null)
        {
            RoleType roleType = GetRoleTypeInChildren(gameObject);
            if(!roleType.Equals(RoleType.Other))
            {
                return roleType;
            }

            gameObject = gameObject.transform.root.gameObject;

            return GetRoleTypeInChildren(gameObject);
        }

        return RoleType.Other;
    }

    public static RoleType GetRoleTypeInChildren(GameObject gameObject)
    {
        if(gameObject != null)
        {
            if(gameObject.name == "Role")
            {
                return (RoleType) Enum.Parse(typeof(RoleType), gameObject.tag);
            }

            foreach(Transform child in gameObject.transform)
            {
                if(child.gameObject.name == "Role")
                {
                    return (RoleType) Enum.Parse(typeof(RoleType), child.gameObject.tag);
                }
            }
        }

        return RoleType.Other;
    }
}