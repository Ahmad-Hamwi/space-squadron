using UnityEngine;

public class DestroyAfterTime : MonoBehaviour {
    public float lifeTime;

    void Start()
    {
        Destroy(gameObject, lifeTime);
    }
}