using UnityEngine;

public class Projectile : MonoBehaviour {
    public float damage;
    public float lifeTime;
    public float speed;
    public float rotationSpeed = 1f;

    public GameObject hitEffectPrefab;

    protected GameObject senderRootGameObject;

    void Start()
    {
        Destroy(gameObject, lifeTime);
    }

    public void SetUp(float damage, float speed, float rotationSpeed, float lifeTime, GameObject senderRootGameObject)
    {
        this.damage = damage;
        this.speed = speed;
        this.rotationSpeed = rotationSpeed;
        this.lifeTime = lifeTime;
        this.senderRootGameObject = senderRootGameObject;
    }

    void FixedUpdate()
    {
        Move();    
    }

    protected virtual void Move()
    {
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    public void SetSenderRootGameObject(GameObject newSenderRootGameObject)
    {
        senderRootGameObject = newSenderRootGameObject;
    }

    protected void OnTriggerEnter(Collider other)
    {
        if(other.transform.root.gameObject != senderRootGameObject)
        {
            if(DamageDealer.dealDamage(senderRootGameObject, other.gameObject, damage))
            {
                Destroy(gameObject);
                HitEffect();
            }
        }
    }

    protected void HitEffect()
    {
        if(hitEffectPrefab != null)
        {
            GameObject hitEffectObject = GameObject.Instantiate(hitEffectPrefab);

            hitEffectObject.transform.position = transform.position;
            hitEffectObject.transform.forward = transform.forward;

            Destroy(hitEffectObject, 1);
        }

        hitEffectPrefab = null;
    }
    
}