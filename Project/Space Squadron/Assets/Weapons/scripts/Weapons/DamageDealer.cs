using UnityEngine;

public class DamageDealer
{
    public static bool dealDamage(GameObject sender, GameObject damagable, float damage)
    {
        RoleType senderRoleType = Role.GetRoleType(sender);
        RoleType damagableRoleType = Role.GetRoleType(damagable);

        if(damagableRoleType.Equals(RoleType.Ally))
        {
            if(senderRoleType.Equals(RoleType.Enemy))
                playerTakingDamage(damagable.GetComponent<PlayerHandler>(), damage);
            return true;
        }
        else if(damagableRoleType.Equals(RoleType.Enemy) || damagableRoleType.Equals(RoleType.SpaceObject) || damagableRoleType.Equals(RoleType.SpaceStation))
        {
            if (senderRoleType.Equals(RoleType.Ally))
            {
                //tell me who shot you
                GameEvents.instance.TargetShooting(sender.name);

                targetTakingDamage(damagable.GetComponent<Target>(), damage);
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public static void playerTakingDamage(PlayerHandler player, float damage)
    {
        if(player != null)
        {
            player.Damage(damage);
        }
    }

    public static void targetTakingDamage(Target target, float damage)
    {
        if(target != null)
        {
            target.Damage(damage);
        }
    }
}