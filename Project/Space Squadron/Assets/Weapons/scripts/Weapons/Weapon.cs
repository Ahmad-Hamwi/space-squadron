using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    public Transform spawnTransform;
    protected Vector3 shootingDirection;
    protected TargetingSystem targetingSystem;
    protected float aimingSpeed;
    public abstract void Shoot();
    public abstract void SetUp(float timeGap, float damage, float speed, float rotationSpeed, float lifeTime);

    protected void FollowTarget()
    {
        if(targetingSystem.lockTarget)
        {   
            spawnTransform.forward = targetingSystem.target - spawnTransform.position;
            shootingDirection = spawnTransform.forward;
        }
        else
        {
            spawnTransform.forward = Vector3.Lerp(spawnTransform.forward, targetingSystem.target - spawnTransform.position, 0.01f * Time.deltaTime);
            shootingDirection = spawnTransform.forward;
        }
    }
}