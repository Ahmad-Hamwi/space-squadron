using UnityEngine;

public class Missile : Projectile
{
    public float startFollowingTime = 0f;

    private Transform target;
    private float startTime;

    RoleType[] targetTypes;

    void Start()
    {
        targetTypes = new RoleType[1]
        {
            RoleType.SpaceObject
        };

        Destroy(gameObject, lifeTime);

        FindTarget();

        startTime = Time.time;
    }

    void FixedUpdate()
    {
        Move();
    }

    protected override void Move()
    {
        rotationSpeed = 1f;
        if(target != null)
        {
            transform.forward = Vector3.Lerp(transform.forward, target.position - transform.position, rotationSpeed * Time.deltaTime);           
        }
        else
        {
            FindTarget();
        }
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    void OnDestroy()
    {
        HitEffect();
    }

    private void FindTarget()
    {
        if(senderRootGameObject != null && targetTypes.Length < 3)
        {
            targetTypes = new RoleType[2]
            {
                Role.GetTargetType(Role.GetRoleType(senderRootGameObject)),
                RoleType.SpaceObject
            };
        }

        target = TargetFinder.GetTarget(transform.gameObject, targetTypes, 250f, 45f);
    }

}