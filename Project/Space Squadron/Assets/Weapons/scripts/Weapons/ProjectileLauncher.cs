using UnityEngine;
using UnityEngine.Events;

public class ProjectileLauncher : Weapon {

    public GameObject projectilePrefab;
    
    public float timeGap = 0.5f;

    public UnityEvent onShoot;

    private float lastFire;

    private Transform rootTransform;

    private float projectileDamage = 10f;
    private float projectileSpeed = 1000f;
    private float projectileRotationSpeed = 0.1f;
    private float projectileLifeTime = 10f;

    void Start()
    {
        lastFire = -Mathf.Infinity;
        rootTransform = transform.root;
        aimingSpeed = Mathf.Infinity;

        targetingSystem = gameObject.GetComponentInParent<TargetingSystem>();
    }

    void FixedUpdate()
    {
        FollowTarget();
    }

    public override void Shoot()
    {
        if(lastFire + timeGap < Time.time)
        {   
            GameObject projectileObject = GameObject.Instantiate(projectilePrefab, spawnTransform.position, spawnTransform.rotation);

            projectileObject.GetComponent<Projectile>().SetUp(projectileDamage, projectileSpeed, projectileRotationSpeed, projectileLifeTime, rootTransform.gameObject);

            projectileObject.transform.forward = targetingSystem.target - transform.position;

            lastFire = Time.time;
            if(onShoot != null)
                onShoot.Invoke();
        }
    }

    public override void SetUp(float timeGap, float damage, float speed, float rotationSpeed, float lifeTime)
    {
        this.timeGap = timeGap;
        this.projectileDamage = damage;
        this.projectileSpeed = speed;
        this.projectileRotationSpeed = rotationSpeed;
        this.projectileLifeTime = lifeTime;
        
    }
}