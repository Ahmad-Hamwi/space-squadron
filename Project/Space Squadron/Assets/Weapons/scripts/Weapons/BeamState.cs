public enum BeamState
{
    Off,
    FadingIn,
    FadingOut,
    Sustaining
}