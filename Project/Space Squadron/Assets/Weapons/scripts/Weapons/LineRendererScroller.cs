using UnityEngine;

public class LineRendererScroller : MonoBehaviour
{
    public LineRenderer lineRenderer;

    public float scrollSpeedX = -5;

    public float tiling = 0.005f;


    protected void Reset()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    private void Update()
    {

        float length = Vector3.Distance(lineRenderer.GetPosition(0), lineRenderer.GetPosition(1));
        float scrollSpeed = scrollSpeedX;
        float nextTiling = tiling * length;

        lineRenderer.material.SetTextureOffset("_MainTex", new Vector2((Time.time * scrollSpeed) % 1.0f, 0f));
        lineRenderer.material.SetTextureScale("_MainTex", new Vector2(nextTiling, 1));

    }
}