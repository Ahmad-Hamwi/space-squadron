﻿using UnityEngine;
using UnityEngine.UI;

public class CrossHair : MonoBehaviour
{
    public CrosshairTarget target;

    public Image crosshairImage;

    public float crosshairUpdateSpeed = 5f;

    public float size = 100f;

    public float offset = 250f;

    public Camera camera;

    public Color defaultColor;
    public Color onTargetingColor;

    RectTransform crosshairRectTransform;
    void Start()
    {
        crosshairImage = transform.GetComponentInChildren<Image>();
        crosshairRectTransform = crosshairImage.GetComponent<RectTransform>();

        crosshairRectTransform.sizeDelta = Vector2.one * size;

        if(target == null)
            target = GameObject.Find("Player").GetComponentInChildren<CrosshairTarget>();
    }

    void FixedUpdate()
    {
        crosshairRectTransform.position = camera.WorldToScreenPoint(target.transform.position + target.toTarget * offset);
        if(target.isTargeting)
        {
            crosshairImage.color = Color.Lerp(crosshairImage.color, onTargetingColor, crosshairUpdateSpeed * Time.deltaTime);
        }
        else 
        {
            crosshairImage.color = Color.Lerp(crosshairImage.color, defaultColor, crosshairUpdateSpeed * Time.deltaTime);
        }
    }
}
