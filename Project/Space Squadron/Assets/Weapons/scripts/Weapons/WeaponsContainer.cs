using UnityEngine;
using System.Collections.Generic;
using Space_Squadron.Assets.Data.Entities.GameData;
using Space_Squadron.Assets.Data.Entities;
public class WeaponsContainer : MonoBehaviour {
    public GameObject projectileLauncherPrefab;
    public GameObject laserBeamPrefab;
    public GameObject missileLauncherPrefab;

    public List<Vector3> primaryWeaponsPositions;
    public List<Vector3> secondaryWeaponsPositions;

    public List<Weapon> primaryWeapons;
    public List<Weapon> secondaryWeapons;

    private WeaponsFactory factroy;

    void Start()
    {
        factroy = GameObject.Find("WeaponsSpawner").GetComponent<WeaponsFactory>();
        primaryWeapons = new List<Weapon>();
        secondaryWeapons = new List<Weapon>();
        
        Vector3 scale =  transform.root.localScale;
        for(int i = 0;i<primaryWeaponsPositions.Count;i++)
        {
            primaryWeaponsPositions[i] = new Vector3(scale.x * primaryWeaponsPositions[i].x,
                                                     scale.y * primaryWeaponsPositions[i].y,
                                                     scale.z * primaryWeaponsPositions[i].z);
        }

        for(int i = 0;i<secondaryWeaponsPositions.Count;i++)
        {
            secondaryWeaponsPositions[i] = new Vector3(scale.x * secondaryWeaponsPositions[i].x,
                                                       scale.y * secondaryWeaponsPositions[i].y,
                                                       scale.z * secondaryWeaponsPositions[i].z);
        }

        SetUp();
    }

    // TODO: read data from file
    private void SetUp()
    {
        // string[] primaryWeaponTypes;

        // string[] secondaryWeaponTypes;

        // if(transform.root.tag == "Player")
        // {
        //     primaryWeaponTypes = new string[3]
        //     {
        //         "BeamWeapon",
        //         "MissileWeapon",
        //         "BeamWeapon"
        //     };

        //     secondaryWeaponTypes = new string[1]
        //     {
        //         "MissileWeapon"
        //     };
        // }
        // else 
        // {
        //     primaryWeaponTypes = new string[3]
        //     {
        //         "none",
        //         "ProjectileWeapon",
        //         "none"
        //     };
        // }

        // for(int i = 0;i<primaryWeaponTypes.Length;i++)
        // {
        //     GameObject weaponPrefab = factroy.GetWeaponByName(primaryWeaponTypes[i]);
            
        //     if(weaponPrefab != null)
        //     {
        //         GameObject weapon = GameObject.Instantiate(weaponPrefab, transform.position + primaryWeaponsPositions[i], transform.rotation, transform);
        //         primaryWeapons.Add(weapon.GetComponent<Weapon>());

        //         if(transform.root.tag != "Player")
        //         {
        //             primaryWeapons[i].SetUp(1, 5, 1000, 0.01f, 2);
        //         }
        //         else
        //         {
        //             if(primaryWeaponTypes[i] != "BeamWeapon")
        //                 primaryWeapons[i].SetUp(0.5f, 5, 1000, 0.01f, 2);
        //             else 
        //                 primaryWeapons[i].SetUp(0.1f, 20, 1000, 0.01f, 500);
        //         }
        //     }
        //     else
        //     {
        //         primaryWeapons.Add(null);
        //     } 
            
        // }

        // /*for(int i = 0;i<secondaryWeaponTypes.Length;i++)
        // {
        //     GameObject weaponPrefab = factroy.GetWeaponByName(secondaryWeaponTypes[i]);
        //     if(weaponPrefab != null)
        //     {
        //         GameObject weapon = GameObject.Instantiate(weaponPrefab);
        //         weapon.transform.position = transform.position + secondaryWeaponsPositions[i];
        //         weapon.transform.rotation = transform.rotation;
        //         weapon.transform.SetParent(gameObject.transform); 
        //         secondaryWeapons.Add(weapon.GetComponent<Weapon>());
        //     }
        //     else
        //     {
        //         secondaryWeapons.Add(null);
        //     } 
        // }*/
        if(transform.root.name == "Player" || transform.root.name == "Player1")
        {
            SetUp(DataManager.instance.GetPlayerSpaceShip());
        }
        else if(transform.root.name == "Player2")
        {
            SetUp(DataManager.instance.GetPlayerSpaceShip());
        }
        else
        {
            SetUpFromNumberOfWeapons(DataManager.instance.GetSceneEntity().enemyBots[0]);
        }
    }

    private string GetWeaponName(BulletType bulletType)
    {
        switch(bulletType)
        {
            case BulletType.FORWARD:
                return "ProjectileWeapon";
            case BulletType.FOLLOW:
                return "MissileWeapon";
            case BulletType.LASER:
                return "BeamWeapon";
        }

        return "none";
    }

    public void FirePrimary()
    {
        for(int i = 0; i < primaryWeapons.Count; i++)
        {
            if(primaryWeapons[i] != null)
                primaryWeapons[i].Shoot();
        }
    }

    public void FireSecondary()
    {
        for(int i = 0; i < secondaryWeapons.Count; i++)
        {
            if(secondaryWeapons[i] != null)
                secondaryWeapons[i].Shoot();
        }
    }

    public void SetUp(SpaceShipEntity spaceShip)
    {
        switch(spaceShip.level)
        {
            case 1:
                SetUpLevel1(spaceShip);
                break;
            case 2:
                SetUpLevel2(spaceShip);
                break;
            case 3:
                SetUpLevel3(spaceShip);
                break;
        }

        if(secondaryWeaponsPositions.Count > 0 && spaceShip.secondaryWeapon != null)
            secondaryWeapons.Add(InstantiateWeapon(spaceShip.secondaryWeapon, secondaryWeaponsPositions[0]));
    }

    public void SetUpFromNumberOfWeapons(SpaceShipEntity spaceShip)
    {
        int weaponsnumber = 0;
        if(spaceShip.primaryWeaponLevel1 != null)
            weaponsnumber++;
        if(spaceShip.primaryWeaponLevel2 != null)
            weaponsnumber++;
        if(spaceShip.primaryWeaponLevel3 != null)
            weaponsnumber++;
        switch(weaponsnumber)
        {
            case 1:
                SetUpLevel1(spaceShip);
                break;
            case 2:
                SetUpLevel2(spaceShip);
                break;
            case 3:
                SetUpLevel3(spaceShip);
                break;
        }
    }
    private void SetUpLevel1(SpaceShipEntity spaceShip)
    {
        if(primaryWeaponsPositions.Count > 1)
            primaryWeapons.Add(InstantiateWeapon(spaceShip.primaryWeaponLevel1, primaryWeaponsPositions[1]));
    }

    private void SetUpLevel2(SpaceShipEntity spaceShip)
    {
        if(primaryWeaponsPositions.Count > 0)
            primaryWeapons.Add(InstantiateWeapon(spaceShip.primaryWeaponLevel1, primaryWeaponsPositions[0]));
        if(primaryWeaponsPositions.Count > 2)
            primaryWeapons.Add(InstantiateWeapon(spaceShip.primaryWeaponLevel2, primaryWeaponsPositions[2]));
    }

    private void SetUpLevel3(SpaceShipEntity spaceShip)
    {
        if(primaryWeaponsPositions.Count > 0)
            primaryWeapons.Add(InstantiateWeapon(spaceShip.primaryWeaponLevel1, primaryWeaponsPositions[0]));
        if(primaryWeaponsPositions.Count > 1)
            primaryWeapons.Add(InstantiateWeapon(spaceShip.primaryWeaponLevel3, primaryWeaponsPositions[1]));
        if(primaryWeaponsPositions.Count > 2)
            primaryWeapons.Add(InstantiateWeapon(spaceShip.primaryWeaponLevel2, primaryWeaponsPositions[2]));
    }

    private Weapon InstantiateWeapon(WeaponEntity weaponEntity, Vector3 position)
    {
        // Debug.Log(weaponEntity.bullet.type);
        // Debug.Log(factroy);
        GameObject weaponPrefab = factroy.GetWeaponByName(GetWeaponName(weaponEntity.bullet.type));
        if(weaponPrefab != null)
        {
            GameObject weaponObject = Instantiate(weaponPrefab, transform.position + position, transform.rotation, transform);
            Weapon weapon = weaponObject.GetComponent<Weapon>();
            if(GetWeaponName(weaponEntity.bullet.type) == "BeamWeapon")
                weapon.SetUp(weaponEntity.timeGap, weaponEntity.bullet.damage, weaponEntity.bullet.speed, weaponEntity.bullet.rotationSpeed, 500);
            else
                weapon.SetUp(weaponEntity.timeGap, weaponEntity.bullet.damage, weaponEntity.bullet.speed, weaponEntity.bullet.rotationSpeed, 1);
            return weapon;
        }
        return null;
    }
}