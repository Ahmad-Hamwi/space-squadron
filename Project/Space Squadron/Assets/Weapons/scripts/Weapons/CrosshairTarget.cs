using UnityEngine;

public class CrosshairTarget : MonoBehaviour {
    public float rotationSpeed = 0.01f;
    public TargetingSystem targetingSystem;
    public bool isTargeting;
    public Vector3 toTarget;

    void Start()
    {
        toTarget = transform.forward;
        rotationSpeed = 0.01f;
    }

    void FixedUpdate()
    {
        if(targetingSystem.lockTarget)
        {   
            transform.forward = targetingSystem.target - transform.position;
            toTarget = transform.forward;
        }
        else
        {
            transform.forward = Vector3.Lerp(transform.forward, targetingSystem.target - transform.position, 0.01f * Time.deltaTime);
            toTarget = transform.forward;
        }
        isTargeting = targetingSystem.isTargeting;
    }
}