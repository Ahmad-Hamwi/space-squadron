using System;
using System.Collections.Generic;
using UnityEngine;

public class WeaponsFactory : MonoBehaviour
{
    [SerializeField] private GameObject[] weaponsPrefabs;
    private Dictionary<string, GameObject> weaponsByNames;


    private void Awake()
    {

        weaponsByNames = new Dictionary<string, GameObject>();

        foreach (GameObject weaponPrefab in weaponsPrefabs)
        {
            String weaponName = weaponPrefab.name;
            weaponsByNames.Add(weaponName, weaponPrefab);
        }
    }

    public GameObject GetWeaponByName(string name)
    {
        if(weaponsByNames.ContainsKey(name))
        {
            return weaponsByNames[name];
        }

        return null;
    }

}
