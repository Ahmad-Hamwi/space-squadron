using UnityEngine;

public class BeamAudioController : MonoBehaviour
{
    public AudioSource beamStartedAudio;

    public AudioSource beamOngoingAudio;

    public float maxBeamOngoingAudioVolume = 1;


    protected virtual void Awake()
    {
        if (beamOngoingAudio != null)
        {
            beamOngoingAudio.volume = 0;
        }
    }

    public virtual void OnBeamStateChanged(BeamState newBeamState)
    {
        if (newBeamState == BeamState.FadingIn)
        {
            if (beamStartedAudio != null)
            {
                beamStartedAudio.Play();
            }
        }
    }

    public virtual void OnBeamLevelSet(float beamLevel)
    {
        if (beamOngoingAudio != null)
        {
            beamOngoingAudio.volume = maxBeamOngoingAudioVolume * beamLevel;
        }
    }
}