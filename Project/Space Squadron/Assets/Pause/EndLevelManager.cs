﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndLevelManager : MonoBehaviour
{
    [SerializeField] private GameObject loseMenu;
    [SerializeField] private GameObject advanceMenu;
    // Start is called before the first frame update


    
    private void OnDisable()
    {
        Time.timeScale = 1;
    }
    public void ShowLoseMenu()
    {
        Time.timeScale = 0;
        //show menu
        loseMenu.SetActive(true);
    }

    public void ShowAdvanceMenu()
    {
        Time.timeScale = 0;

        //set values
        SetAdvanceMenuValues();
        
        
        //show menu
        advanceMenu.SetActive(true);
    }

    private void SetAdvanceMenuValues()
    {
        Text[] texts = advanceMenu.GetComponentsInChildren<Text>();
        foreach (var text in texts)
        {
            if (text.CompareTag("Score"))
            {
                int score = GameObject.Find("GameManager").GetComponentInChildren<ScoreManager>().currentScore;
                text.text = score.ToString("D9");
            }
            if (text.CompareTag("Bots"))
            {
                int kills = GameObject.Find("GameManager").GetComponentInChildren<ScoreManager>().kills;
                text.text = kills.ToString();       
            }
            if (text.CompareTag("Stations"))
            {

            }
            if (text.CompareTag("Coins"))
            {
                int coins = GameObject.Find("GameManager").GetComponentInChildren<CoinsManager>().GetCoins();
                text.text = coins.ToString();
            }
        }
    }
}
