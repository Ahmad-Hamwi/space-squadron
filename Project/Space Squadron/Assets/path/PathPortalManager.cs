﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathPortalManager : MonoBehaviour
{
    public GameObject portalPrefab;

    [SerializeField] private float endLevelTime;
    [SerializeField] public float timeToSpawn { get; set; }
    [SerializeField] public float spawnAfter { get; set; }
    [SerializeField] private Vector3 offset;

    bool spawned = false;

    private void Awake()
    {
        this.spawnAfter = endLevelTime;
        this.timeToSpawn = this.spawnAfter;
        offset = new Vector3(0, 75, 0);
    }

    // Start is called before the first frame update
    void Start()
    {
        
        GameEvents.instance.onSpawnPortal += Instance_onSpawnPortal;
    }

    private void Instance_onSpawnPortal()
    {
        Vector3 pos = GetLastPosition();
        SpawnPortal(pos);
    }

    private void OnDisable()
    {
        GameEvents.instance.onSpawnPortal -= Instance_onSpawnPortal;
    }

    // Update is called once per frame
    void Update()
    {
        timeToSpawn -= Time.deltaTime;
        if (timeToSpawn <= 0 && !spawned)
        {
            GameEvents.instance.SpawnPortal();
            spawned = true;
        }
    }

    private void SpawnPortal(Vector3 pos)
    {
        Debug.Log("Portal Shall be Spawned");
        Instantiate(portalPrefab, pos + offset, Quaternion.Euler(0, 0, 0));
        
    }

    private Vector3 GetLastPosition()
    {
        Vector3 pos = GameObject.Find("Path Control").GetComponent<PathController>().GetLastPosition();
        return pos;
    }
}
