﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class percentageIndicator : MonoBehaviour
{
    [SerializeField] private float spawnTime;
    [SerializeField] private float timer;
    [SerializeField] Slider indicator;

    // Start is called before the first frame update
    void Start()
    {
        spawnTime = GameObject.Find("PathPortalManager").GetComponent<PathPortalManager>().spawnAfter;
        indicator = gameObject.GetComponent<Slider>();
        indicator.value = 0f;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        
        this.timer = GameObject.Find("PathPortalManager").GetComponent<PathPortalManager>().timeToSpawn;
        this.indicator.value = 100 - (float)(this.timer / this.spawnTime)*100;
    }
}
