﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class PathController : MonoBehaviour
{


	//The Default grounds
	private List<GameObject> defaultPathList;

	//The positions of the grounds
	private List<Vector3> defaultPathPosList;

	//All The paths (where we add and remove frequently).
	private LinkedList<GameObject> paths;

	[SerializeField] private int numberOfPathPieces = 7;

	//private Route next;

	// Use this for initialization
	void Start()
	{
		Application.targetFrameRate = 60;

		this.defaultPathList = new List<GameObject>();
		this.defaultPathPosList = new List<Vector3>();
		this.paths = new LinkedList<GameObject>();

		//adjust the path
		SetUpPath();

		for (int i = 1; i <= numberOfPathPieces; i++)
		{
			GameObject path = GameObject.Find("PathUnit" + i);




			//Add to the default path list
			defaultPathList.Add(path);
			//Add to the list of positions
			defaultPathPosList.Add(path.transform.position);
			//Add to the editable list
			paths.AddLast(path);
		}
	}

	private void SetUpPath()
	{
		for (int i = 0; i < numberOfPathPieces; i++)
		{
			GameObject path = GameObject.Find("PathUnit" + (i + 1));
			//Set the position to be correct
			path.transform.position = new Vector3(0, 0, i * 100);
			foreach (MeshRenderer mr in path.GetComponentsInChildren<MeshRenderer>())
			{
				//hide all the meshs
				mr.enabled = false;
			}
		}

	}

	public void resetGame()
	{
		// re set all the positions to the path objects
		for (int i = 0; i < this.defaultPathList.Count; i++)
		{
			GameObject path = this.defaultPathList[i];
			path.transform.position = this.defaultPathPosList[i];
			path.transform.rotation = Quaternion.identity;
		}
		Start();
	}


	public void recycle()
	{
		GameObject first = this.paths.First.Value;
		GameObject last = this.paths.Last.Value;
		this.paths.Remove(first);
		this.paths.AddLast(first);
		first.transform.position = last.transform.position + new Vector3(0, 0, 100);

		//Reset the spawning position of the objects.
		GameObject.Find("PathAsteroidsSpawner").GetComponent<PathAsteroidSpawner>().Spawn(first.transform);
		GameObject.Find("PathBotsSpawner").GetComponent<PathBotsSpawner>().currentPathTransform = first.transform;

	}

	public Vector3 GetLastPosition()
	{
		return this.paths.Last.Value.transform.position;
	}
}
