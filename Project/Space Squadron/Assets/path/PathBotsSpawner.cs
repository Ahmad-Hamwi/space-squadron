using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class PathBotsSpawner : MonoBehaviour
{

    public GameObject[] prefabs;
    public Vector3 pathFormations = new Vector3(300, 150, 100);

    public Camera myCamera;

    public float myCameraToCenter = 100f;

    public Transform currentPathTransform;

    [SerializeField] private int objects = 2;

    [SerializeField] private int minScale = 3;
    [SerializeField] private int maxSclae = 5;

    [SerializeField] private float minTimeBetweenSpawns = 0.1f;
    [SerializeField] private float maxTimeBetweenSpawns = 10f;

    private bool portalSpawned = false;

    private float nextSpawnTime;

    void Start()
    {
        nextSpawnTime = 1;
        GameEvents.instance.onSpawnPortal += Instance_onSpawnPortal;
    }

    private void Instance_onSpawnPortal()
    {
        portalSpawned = true;
    }

    void Update()
    {
        if(Time.time > nextSpawnTime)
        {
            Spawn();
            nextSpawnTime = Time.time + Random.Range(minTimeBetweenSpawns, maxTimeBetweenSpawns);
        }
    }

    public void Spawn()
    {
        if(currentPathTransform != null && ! portalSpawned)
        {
            Vector3 center = currentPathTransform.position;
            for (int objsCounter = 0; objsCounter < objects; objsCounter++)
            {
                Vector3 spawnPosition = new Vector3(
                    UnityEngine.Random.Range(center.x - pathFormations.x / 2, center.x + pathFormations.x / 2),
                    UnityEngine.Random.Range(0, center.y + pathFormations.y),
                    UnityEngine.Random.Range(center.z - pathFormations.z / 2, center.z + pathFormations.z / 2)
                    );
                int index = UnityEngine.Random.Range(0, prefabs.Length);

                GameObject bot = Instantiate(prefabs[index], spawnPosition, prefabs[index].transform.rotation);

                Destroy(bot, 10);
                int scale = UnityEngine.Random.Range(minScale, maxSclae);
                bot.transform.localScale = new Vector3(scale, scale, scale);
            }
        }
    }
}
