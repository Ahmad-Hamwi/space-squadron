﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class PathAsteroidSpawner : MonoBehaviour
{

    public GameObject[] prefabs = new GameObject[3];
    public Vector3 pathFormations = new Vector3(300, 150, 100);
    [SerializeField] private int objects = 2;

    [SerializeField] private int minScale = 5;
    [SerializeField] private int maxSclae = 10;

    private bool portalSpawned = false;
    private void Start()
    {
        GameEvents.instance.onSpawnPortal += Instance_onSpawnPortal;
    }

    private void Instance_onSpawnPortal()
    {
        portalSpawned = true;
    }

    public void Spawn(Transform pathObj)
    {
        if (portalSpawned)
        {
            emptyPath(pathObj);
            return;
        }


        Vector3 center = pathObj.position;
        int astNum = 0;
        if (pathObj.GetComponentsInChildren<Astroid>() != null)
        {
            astNum = pathObj.GetComponentsInChildren<Astroid>().Length;
        }

        while(astNum < objects)
        {
            Vector3 spawnPosition = new Vector3(
                UnityEngine.Random.Range(center.x - pathFormations.x / 2, center.x + pathFormations.x / 2),
                UnityEngine.Random.Range(0, center.y + pathFormations.y),
                UnityEngine.Random.Range(center.z - pathFormations.z / 2, center.z + pathFormations.z / 2)
                );
            int index = UnityEngine.Random.Range(0, prefabs.Length);

            
            

            GameObject astroid = Instantiate(prefabs[index], spawnPosition, Quaternion.identity, pathObj);
            // GameObject astroid = Instantiate(prefabs[index]);
            // astroid.transform.position = spawnPosition;
            int scale = UnityEngine.Random.Range(minScale, maxSclae);
            astroid.transform.localScale = new Vector3(scale, scale, scale);
            astNum++;
        }
    }

    private void emptyPath(Transform pathObj)
    {
        foreach (var asteroid in pathObj.GetComponentsInChildren<Astroid>())
        {
            GameObject.Destroy(asteroid.gameObject);
        }
    }
}
