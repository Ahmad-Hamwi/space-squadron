﻿using UnityEngine;

public sealed class ParticleFollow : MonoBehaviour
{
	private Transform cachedTransform;
	private ParticleSystemRenderer particleRenderer;
	[SerializeField] private float positionSmooth;
	[SerializeField] private float rotatoionSmooth;
	[SerializeField] private Transform target;

	private void Awake()
	{
		particleRenderer = GetComponent<ParticleSystemRenderer>();
	}

	private void Update()
	{
		transform.position = Vector3.Lerp(transform.position,
			target.position, Time.deltaTime * positionSmooth);

		transform.rotation = Quaternion.Slerp(transform.rotation,
			target.rotation, Time.deltaTime * rotatoionSmooth);
		
		particleRenderer.lengthScale = -1;

	}
}
