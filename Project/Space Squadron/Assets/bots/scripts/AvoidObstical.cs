using UnityEngine;
using System.Collections.Generic;

public class AvoidObstical : BaseMoveUtil 
{

    private float scanningRadius;
    private float lastChange;
    private float effectPeriod;
    private Vector3 steer;
    private float radius;
    private Engine movingObjectEngine;
    private Vector3 lastEffect;

    public AvoidObstical(float scanningRadius, float effectPeriod, float radius, Engine movingObjectEngine)
    {
        this.scanningRadius = scanningRadius;
        this.effectPeriod = effectPeriod;
        this.radius = radius;
        this.movingObjectEngine = movingObjectEngine;
        this.steer = movingObjectEngine.transform.forward;

        lastChange = -Mathf.Infinity;
    }
    public override void PerformMove() {

        RaycastHit[] hit = Rays(movingObjectEngine.transform.forward);

        int index = -1;
        float dist = 0;

        for(int i = 0; i < hit.Length; i++)
        {
            if(hit[i].transform.root.gameObject != movingObjectEngine.transform.root.gameObject) {
                if(dist > (hit[i].collider.transform.position - movingObjectEngine.transform.position).magnitude || index == -1) {
                    index = i;
                    dist = (hit[i].collider.transform.position - movingObjectEngine.transform.position).magnitude;
                }
            }
        }

        if(index == -1) 
        {
            steer = Vector3.Lerp(steer, Vector3.zero, 50 * Time.deltaTime);
        }

        else
        {
            Vector3 ahead = hit[index].point;
            steer = ahead - hit[index].transform.position;
        }

        if(steer != Vector3.zero)
            movingObjectEngine.AcceptTurnning(Vector3.Cross(movingObjectEngine.transform.forward, steer), steer);
    }

    RaycastHit[] Rays(Vector3 direction)
    {
        float distanceToLookAhead = 1000f;
        
        RaycastHit[] hits = Physics.SphereCastAll(movingObjectEngine.transform.position, radius, direction.normalized, distanceToLookAhead);

        return hits;        
    }
}