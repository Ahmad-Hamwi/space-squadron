using UnityEngine;

public class PathBotBehavior : MonoBehaviour {
    private PathState currentState;

    private BaseMoveUtil obstacleAvoiding;

    private Engine engine;

    private Vector3 startingForward;

    private float startCorrecting;
    private float correctingTime;

    private WeaponsContainer weapons;

    void Start()
    {
        engine = gameObject.GetComponent<Engine>();

        obstacleAvoiding = new PathObstacleAvoiding(10, 3, 10, gameObject.GetComponent<Engine>());

        startingForward = transform.forward;

        weapons = gameObject.GetComponentInChildren<WeaponsContainer>();
        weapons.transform.GetComponent<TargetingSystem>().aimAngle = 30;
        weapons.transform.GetComponent<TargetingSystem>().targetTypes = new RoleType[1]
        {
            Role.GetTargetType(Role.GetRoleType(gameObject))
        };

        engine.SetRotationSpeedManger(100f, 50f);
        engine.SetTranlationSpeedManger(0, 50f);
    }

    void Update()
    {
        Vector3 currentForward = transform.forward;

        obstacleAvoiding.PerformMove();

        engine.AcceptTranslation();
        weapons.FirePrimary();       
    }
}