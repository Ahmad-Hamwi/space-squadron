public interface PathState
{
    void OnStateEnter();
    void Update();
}