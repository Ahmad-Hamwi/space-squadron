using UnityEngine;

public class OutOfRangeState : OpenWorldState
{

    private Transform bot;
    private Engine botEngine;
    private Vector3 cinter;
    private float startPatrolRadius;
    private TurnToward backToCenterMove;
    private float dist;
    private OpenWorldBotBehavior behavior;

    public OutOfRangeState(Transform bot, Engine botEngine, OpenWorldBotBehavior behavior)
    {
        this.bot = bot;
        this.botEngine = botEngine;
        this.behavior = behavior;

        this.cinter = behavior.GetHomeCinter();
        this.startPatrolRadius = behavior.GetStartPatrolRadius();
        this.dist = behavior.GetDist();
    }

    public void OnStateEnter()
    {
        backToCenterMove = new TurnToward(behavior.GetHomeCinter(), botEngine);
    }

    public void Update()
    {
        Move();

        if(Vector3.Distance(TargetFinder.FindNearestTarget(bot.root.gameObject).position,  bot.position) < dist)
        {
            behavior.setState(new OffensiveState(bot, botEngine, behavior.GetAttackTimeRange(), behavior));
        }
        else if(Vector3.Distance(bot.position, cinter) < startPatrolRadius)
        {
            behavior.setState(new PatrolState(bot, botEngine, behavior));
        }
    }

    public void Move()
    {
        backToCenterMove.PerformMove();
        botEngine.AcceptTranslation();
    }
}