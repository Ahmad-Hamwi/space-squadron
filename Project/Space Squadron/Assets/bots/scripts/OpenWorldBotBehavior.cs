﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenWorldBotBehavior : MonoBehaviour
{    
    // patrol info 
    [SerializeField]
    private float homeRadius = 1000f;
    private Vector3 homeCinter; 

    // compact info
    [SerializeField]
    private float dist = 500f;
    [SerializeField]
    private float dangerDistance = 100f;
    [SerializeField]
    private Vector2 attackTimeRange = new Vector2(10f, 20f);
    [SerializeField]
    private Vector2 evadeTimeRange = new Vector2(5f, 10f);
    [SerializeField]
    private float evadeAngle = 90f;
    [SerializeField]
    private float returnToHomeFactor = 0.0001f;
    [SerializeField]
    private WeaponsContainer weapons;

    // obstacle avoiding info
    [SerializeField]
    private float scanningRadius = 1000;
    [SerializeField]
    private float steeringForceConservation = 0.9f;
    [SerializeField]
    private float radius = 30;

    BaseMoveUtil obstacleAvoiding;

    // patrol info
    [SerializeField]
    private float randomPointFrequency = 5;
    [SerializeField]
    private float wanderRadius = 20;
    [SerializeField]
    private float wanderDistance = 70;
    [SerializeField]
    public WayPointsSystem patrolPath { get; set; }
    [SerializeField]
    private float startPatrolRadius = 500f;


    private OpenWorldState currentState;


    void Start()
    {
        obstacleAvoiding = new AvoidObstical(10, 3, 10, gameObject.GetComponent<Engine>());
        homeCinter = transform.position;
        setState(new PatrolState(transform, gameObject.GetComponent<Engine>(), this));  
        weapons.transform.GetComponent<TargetingSystem>().targetTypes = new RoleType[1]
        {
            Role.GetTargetType(Role.GetRoleType(gameObject))
        };
        gameObject.GetComponent<Engine>().SetRotationSpeedManger(DataManager.instance.GetSceneEntity().enemyBots[0].minSpeed, DataManager.instance.GetSceneEntity().enemyBots[0].maxSpeed);      
    }

    
    void Update()
    {
        //Debug.Log(currentState);
        obstacleAvoiding.PerformMove();
        currentState.Update();
    }

    public void setState(OpenWorldState newState) 
    {
        currentState = newState;
        currentState.OnStateEnter();
    }

    public float GetDangerDistance() => dangerDistance;
    public float GetHomeRadius() => homeRadius;
    public Vector3 GetHomeCinter() => homeCinter;
    public Vector2 GetAttackTimeRange() => attackTimeRange;
    public Vector2 GetEvadeTimeRange() => evadeTimeRange;
    public float GetEvadeAngle() => evadeAngle;
    public float GectReturnToHomeFactor() => returnToHomeFactor;
    public WeaponsContainer GetWeapons() => weapons;
    public float GetWanderRadius() => wanderRadius;
    public float GetWanderDistance() => wanderDistance;
    public float GetRandomPointFrequency() => randomPointFrequency;
    public float GetDist() => dist;
    public float GetStartPatrolRadius() => startPatrolRadius;
    public WayPointsSystem GetPatrolPath() => patrolPath;
}
