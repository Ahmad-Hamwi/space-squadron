// using UnityEngine;

// public class Wander : BaseMoveUtil
// {
    
//     private Vector3 randomPoint;
//     private Vector3 wanderCirclePosition;
//     private float timer;
//     private float randomPointFrequency;
//     private float wanderRadius;
//     private float wanderDistance;

//     public Wander(float wanderRadius, float wanderDistance, float randomPointFrequency)
//     {
//         this.wanderRadius = wanderRadius;
//         this.wanderDistance = wanderDistance;
//         this.randomPointFrequency = randomPointFrequency;

//         timer = Mathf.Infinity;
//     }

//     public override void PerformMove(Transform movingObject, Engine movingObjectEngine)
//     {
//         wanderCirclePosition = movingObject.position + movingObject.forward * wanderDistance;

//         UpdateTimer();

//         Vector3 desiredVelocity = randomPoint - movingObject.position - movingObject.forward;
//         movingObjectEngine.AddSteer(desiredVelocity);

//         if(Vector3.Distance(randomPoint, movingObject.position) < 30)
//         {
//             randomPoint += movingObject.forward * wanderDistance;
//         }
//     }

//     private void UpdateTimer() {
//         timer += Time.deltaTime;

//         if (timer > randomPointFrequency)
//         {
//             randomPoint = Random.insideUnitSphere * wanderRadius;
//             randomPoint += wanderCirclePosition;
//             timer = 0;
//         }
//     }
// }