using UnityEngine;

public class OffensiveState : OpenWorldState 
{

    private OpenWorldBotBehavior behavior;
    // private BaseMoveUtil followTarget;
    private float startTime;
    private float attackPeriod;
    private Vector2 attackTimeRange;
    private Transform bot;
    private Engine botEngine;
    private Transform target;
    private float homeRadius;
    private Vector3 homeCinter;
    private WeaponsContainer weapons;

    public OffensiveState(Transform bot, Engine botEngine, Vector2 attackTimeRange, OpenWorldBotBehavior behavior) 
    {
        this.bot = bot;
        this.botEngine = botEngine;
        this.attackTimeRange = attackTimeRange;
        this.behavior = behavior;
        this.target = TargetFinder.FindNearestTarget(bot.root.gameObject).transform;
        this.weapons = behavior.GetWeapons();
    }
    public void OnStateEnter()
    {
        // followTarget = new TurnToward(target.position);
        startTime = Time.time;
        attackPeriod = Random.Range(attackTimeRange.x, attackTimeRange.y);

        homeRadius = behavior.GetHomeRadius();
        homeCinter = behavior.GetHomeCinter();

    }
    
    public void Update() 
    {
        Move();
        Shoot();

        if(Time.time > startTime + attackPeriod)
        {   
            if(Vector3.Distance(bot.position, homeCinter) > homeRadius)
            {
                behavior.setState(new OutOfRangeState(bot, botEngine, behavior));
            }
            else if(Vector3.Distance(bot.position, TargetFinder.FindNearestTarget(bot.root.gameObject).position) > behavior.GetDist())
            {
                behavior.setState(new PatrolState(bot, botEngine, behavior));
            }
            startTime = Time.time;
        }     
          
        if(Vector3.Distance(bot.position, TargetFinder.FindNearestTarget(bot.root.gameObject).position) < behavior.GetDangerDistance()) 
        {
            behavior.setState(new DefensiveState(bot, botEngine, behavior.GetEvadeTimeRange(), behavior.GetEvadeAngle(), behavior.GectReturnToHomeFactor(), behavior));
        }
        
    }

    public void Move()
    {
        (new TurnToward(TargetFinder.FindNearestTarget(bot.root.gameObject).position, botEngine)).PerformMove();
        botEngine.AcceptTranslation();
    }

    public void Shoot()
    {
        weapons.FirePrimary();
    }

    private Vector3 Weave(Vector3 pathDirection)
    {
        float offsetX = (Mathf.PerlinNoise(Time.time, 0f) - 0.5f) * 2f;

        float offsetY = (Mathf.PerlinNoise(0f, Time.time) - 0.5f) * 2f;

        Vector3 offsetVec = new Vector3(offsetX, offsetY, 1).normalized * 2.5f;
        Vector3 offset = new Vector3(offsetVec.x, offsetVec.y, 1).normalized;

        pathDirection = Quaternion.FromToRotation(Vector3.forward, pathDirection) * offset;

        return pathDirection;

    }
}