﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public GameObject bulletPrefabRight;
    public GameObject falshToPlayWhenShooting;
    public float timeGap = 0.01f;
    public int inFront = 2;

    private float instTime; 

    
    // Start is called before the first frame update
    void Start() {
        instTime = -Mathf.Infinity;
    }

    public void shoot() 
    {
        if(instTime + timeGap < Time.time)
        {
            GameObject bulletObj = Instantiate(bulletPrefabRight);
            bulletObj.transform.position = transform.position + transform.forward * inFront;
            bulletObj.transform.forward = transform.forward;
            instTime = Time.time;

            if(falshToPlayWhenShooting) {
                Instantiate(falshToPlayWhenShooting, transform.position, transform.rotation, transform);
            }
        }
    }

    public void shootAt(Vector3 target)
    {
        if(instTime + timeGap < Time.time)
        {
            GameObject bulletObj = Instantiate(bulletPrefabRight);
            bulletObj.transform.position = transform.position + transform.forward * inFront;
            bulletObj.transform.forward = target - bulletObj.transform.position;
            instTime = Time.time;

            if(falshToPlayWhenShooting) {
                Instantiate(falshToPlayWhenShooting, transform.position, transform.rotation, transform);
            }
        }
    }
}
