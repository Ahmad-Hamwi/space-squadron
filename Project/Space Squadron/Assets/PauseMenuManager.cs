﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuManager : MonoBehaviour
{

    [SerializeField] private GameObject pauseMenu;

    private void Awake()
    {
        
    }

    public void PauseUnPauseGame()
    {
        pauseMenu.SetActive(pauseMenu.active ? false : true);
    }
}
