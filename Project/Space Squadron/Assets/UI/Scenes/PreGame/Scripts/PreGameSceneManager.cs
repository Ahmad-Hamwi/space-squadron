﻿using System.Linq.Expressions;
using System.Collections;
using System.Collections.Generic;
using Space_Squadron.Assets.Data.Constants;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;
using Space_Squadron.Assets.Data.Repositories.Settings;
using Space_Squadron.Assets.Data.Entities;
using System;
using UnityEngine.EventSystems;

public class PreGameSceneManager : MonoBehaviour
{

    [SerializeField]
    private GameObject IntroMenu;

    [SerializeField]
    private GameObject ChoosePlayerMenu;

    [SerializeField]
    private GameObject NewPlayerDialog;

    [SerializeField]
    private GameObject NavigationMenu;

    [SerializeField]
    private GameObject SettingsMenu;

    [SerializeField]
    private GameObject StoreMenu;

    [SerializeField]
    private GameObject ChooseLevelMenu;

    [SerializeField]
    private GameObject ChooseSecondPlayerSpaceShip;

    [SerializeField]
    private AudioMixer mixer;

    [SerializeField]
    private Animator LoadLevelAnimator;

    private SettingsRepository settingsRespository;

    private void Awake()
    {
        
    }

    private void Start() {

        AddPhysicsRayCaster();

        settingsRespository = SettingsRepositoryImpl.GetInstance();

        MenuEvents.instance.onPlay += transitionFromIntroToChoosePlayer;

        MenuEvents.instance.onGameExit += endGame;

        MenuEvents.instance.onNewPlayer += ShowNewPlayerDialog;

        MenuEvents.instance.onPlayerChosen += TransitionFromChoosePlayerToNavigation;

        MenuEvents.instance.onSettingsApply += TransitionFromSettingsToNavigation;

        MenuEvents.instance.onSettingsCanceled += TransitionFromSettingsToNavigation;

        setup();
    }

    private void AddPhysicsRayCaster()
    {
        PhysicsRaycaster physicsRaycaster = GameObject.FindObjectOfType<PhysicsRaycaster>();
        if (physicsRaycaster == null)
        {
            Camera.main.gameObject.AddComponent<PhysicsRaycaster>();
        }
    }

    public void transitionFromIntroToChoosePlayer() {
        IntroMenu.SetActive(false);
        ChoosePlayerMenu.SetActive(true);
    }

    public void TransitionFromChoosePlayerToIntro() {
        ChoosePlayerMenu.SetActive(false);
        IntroMenu.SetActive(true);
    }

    public void endGame() {
        Application.Quit();
    }

    public void ShowNewPlayerDialog() {
        NewPlayerDialog.SetActive(true);
    }

    public void HideShowNewPlayerDialog() {
        NewPlayerDialog.SetActive(false);
    }

    public void TransitionFromChoosePlayerToNavigation()
    {
        //Debug.Log("player has been chosen and the name is " + PlayerPrefs.GetString(PlayerPrefsKeys.CURRENT_PLAYER_NAME));
        NewPlayerDialog.SetActive(false);
        ChoosePlayerMenu.SetActive(false);
        NavigationMenu.SetActive(true);
    }

    public void TransitionFromNavigationToSettings() {
        NavigationMenu.SetActive(false);
        SettingsMenu.SetActive(true);
    }

    public void TransitionFromSettingsToNavigation() {
        SettingsMenu.SetActive(false);
        NavigationMenu.SetActive(true);
    }

    public void TransitionFromNavigationToChoosePlayer() {
        NavigationMenu.SetActive(false);
        ChoosePlayerMenu.SetActive(true);
    }

    public void TransitionToScene1()
    {
        PlayerPrefs.SetInt(PlayerPrefsKeys.BACK_TO_MENUS, 1);
        StartCoroutine(LoadLevel(1));
    }

    public void TransitionToScene2()
    {
        PlayerPrefs.SetInt(PlayerPrefsKeys.BACK_TO_MENUS, 1);
        StartCoroutine(LoadLevel(2));
    }

    public void TransitionToMulti()
    {
        PlayerPrefs.SetInt(PlayerPrefsKeys.BACK_TO_MENUS, 1);
        StartCoroutine(LoadLevel(3));
    }

    public void TransitionToScene(int levelIndex)
    {
        PlayerPrefs.SetInt(PlayerPrefsKeys.BACK_TO_MENUS, 1);
        StartCoroutine(LoadLevel(levelIndex));
    }

    public IEnumerator LoadLevel(int levelIndex)
    {
        LoadLevelAnimator.SetTrigger("load_level");
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(levelIndex);
    }

    public void TransitionFromNavigationToStore()
    {
        NavigationMenu.SetActive(false);
        StoreMenu.SetActive(true);
    }

    public void TransitionFromStoreToNavigation()
    {
        StoreMenu.SetActive(false);
        NavigationMenu.SetActive(true);
    }

    public void TransitionFromNavigationToChooseLevel()
    {
        NavigationMenu.SetActive(false);
        ChooseLevelMenu.SetActive(true);
    }

    public void TransitionFromChooseLevelToNavigation()
    {
        ChooseLevelMenu.SetActive(false);
        NavigationMenu.SetActive(true);
    }

    public void TransitionFromNavigationToChooseSecondPlayer()
    {
        NavigationMenu.SetActive(false);
        ChooseSecondPlayerSpaceShip.SetActive(true);
    }
    public void TransitionFromChooseSecondPlayerToNavigation()
    {
        ChooseSecondPlayerSpaceShip.SetActive(false);
        NavigationMenu.SetActive(true);
    }
    public void setup()
    {
        if(PlayerPrefs.GetInt(PlayerPrefsKeys.BACK_TO_MENUS, 0) == 0)
        {
            IntroMenu.SetActive(true);
            ChoosePlayerMenu.SetActive(false);
            NewPlayerDialog.SetActive(false);
            NavigationMenu.SetActive(false);
            SettingsMenu.SetActive(false);
            StoreMenu.SetActive(false);
            ChooseLevelMenu.SetActive(false);
            ChooseSecondPlayerSpaceShip.SetActive(false);
        } else
        {
            IntroMenu.SetActive(false);
            ChoosePlayerMenu.SetActive(false);
            NewPlayerDialog.SetActive(false);
            NavigationMenu.SetActive(true);
            SettingsMenu.SetActive(false);
            StoreMenu.SetActive(false);
            ChooseLevelMenu.SetActive(false);
            ChooseSecondPlayerSpaceShip.SetActive(false);
        }
        PlayerPrefs.SetInt(PlayerPrefsKeys.BACK_TO_MENUS, 0);
    }
}
