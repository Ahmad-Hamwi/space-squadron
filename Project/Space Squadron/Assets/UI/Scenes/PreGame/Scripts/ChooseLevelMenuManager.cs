﻿using Assets.UI.Data.Constants;
using Space_Squadron.Assets.Data.Constants;
using Space_Squadron.Assets.Data.Repositories;
using Space_Squadron.Assets.Data.Repositories.ChooseLevel;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChooseLevelMenuManager : MonoBehaviour
{
    [SerializeField] private Animator levelAnimator;

    private int currentLevel;

    [SerializeField] private GameObject ChoosePlayerCountGameObject;

    private ChooseLevelRepository choosePlayerRepository;

    private int lastLevelReachedIndex;

    [SerializeField] private Button Level1Button;

    [SerializeField] private Button Level2Button;
                                         
    [SerializeField] private Button Level3Button;
                                         
    [SerializeField] private Button Level4Button;
                                         
    [SerializeField] private Button Level5Button;
                                         
    [SerializeField] private Button Level6Button;

    [SerializeField] private Button Level7Button;

    [SerializeField] private GameObject sceneManager;

    private void Awake()
    {
        choosePlayerRepository = ChooseLevelRepositoryImpl.GetInstance();
    }

    private void OnEnable()
    {
        currentLevel = 1;
        lastLevelReachedIndex = choosePlayerRepository.LoadPlayerLevelProgressIndex();

        levelAnimator.Play("level_1_idle", -1, 0f);

        ShowAvailableLevels();
    }

    private void ShowAvailableLevels()
    {
        Level1Button.enabled = true;
        if (lastLevelReachedIndex > 1) Level2Button.interactable= true;
        if (lastLevelReachedIndex > 2) Level3Button.interactable = true;
        if (lastLevelReachedIndex > 3) Level4Button.interactable = true;
        if (lastLevelReachedIndex > 4) Level5Button.interactable = true;
        if (lastLevelReachedIndex > 5) Level6Button.interactable = true;
        if (lastLevelReachedIndex > 6) Level7Button.interactable = true;
    }

    public void OnLevelForward()
    {
        if (currentLevel == GameConstants.MAX_GAME_LEVELS) return;
        currentLevel++;
        string trigger = $"level_{currentLevel}_open";
        levelAnimator.SetTrigger(trigger);
        ChoosePlayerCountGameObject.SetActive(false);
    }

    public void OnLevelBackward()
    {
        if (currentLevel == 1) return;
        string trigger = $"level_{currentLevel}_close";
        levelAnimator.SetTrigger(trigger);
        currentLevel--;
        ChoosePlayerCountGameObject.SetActive(false);
    }

    public void OnChoosePathLevel(int index)
    {
        ChoosePlayerCountGameObject.SetActive(true);
    }

    public void OnPlayerCountChosen(int playerCount)
    {
        if (playerCount == 1)
        {
            PlayerPrefs.SetInt(PlayerPrefsKeys.CURRENT_PLAYER_COUNT, 1);
            switch (currentLevel)
            {
                case 1:
                    sceneManager.GetComponent<PreGameSceneManager>().LoadLevel(1);
                    break;
                case 2:
                    sceneManager.GetComponent<PreGameSceneManager>().LoadLevel(2);
                    break;
                case 3:
                    sceneManager.GetComponent<PreGameSceneManager>().LoadLevel(4);
                    break;
                case 4:
                    sceneManager.GetComponent<PreGameSceneManager>().LoadLevel(5);
                    break;
                case 5:
                    sceneManager.GetComponent<PreGameSceneManager>().LoadLevel(7);
                    break;
                case 6:
                    sceneManager.GetComponent<PreGameSceneManager>().LoadLevel(8);
                    break;
                case 7:
                    sceneManager.GetComponent<PreGameSceneManager>().LoadLevel(10);
                    break;
            }
        } else if (playerCount == 2)
        {
            PlayerPrefs.SetInt(PlayerPrefsKeys.CURRENT_PLAYER_COUNT, 2);
            switch (currentLevel)
            {
                case 2:
                    sceneManager.GetComponent<PreGameSceneManager>().LoadLevel(3);
                    break;
                case 4:
                    sceneManager.GetComponent<PreGameSceneManager>().LoadLevel(6);
                    break;
                case 6:
                    sceneManager.GetComponent<PreGameSceneManager>().LoadLevel(9);
                    break;
            }

        }
    }
    
}
