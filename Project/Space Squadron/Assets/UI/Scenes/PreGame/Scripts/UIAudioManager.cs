﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAudioManager : MonoBehaviour
{

    [SerializeField] private AudioSource click;
    [SerializeField] private AudioSource change;
    [SerializeField] private AudioSource back;
    [SerializeField] private AudioSource levelLoad;


    public void PlayClickSound()
    {
        click.Play();
    }

    public void PlayChangeSound()
    {
        change.Play();
    }

    public void PlayBackSound()
    {
        back.Play();
    }

    public void PlayLevelLoadSound()
    {
        levelLoad.Play();
    }
}
