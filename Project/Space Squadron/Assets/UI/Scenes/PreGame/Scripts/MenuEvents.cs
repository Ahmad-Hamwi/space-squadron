﻿using System.Reflection.Emit;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuEvents : MonoBehaviour
{
    public static MenuEvents instance;

    private void Awake() {
        instance = this;
    }

    public event Action onPlay;
    public event Action onGameExit;
    public event Action onNewPlayer;
    public event Action onPlayerChosen;
    public event Action onSettings;
    public event Action onSettingsApply;
    public event Action onSettingsCanceled;
    
    public void Play() {
        onPlay?.Invoke();
    }

    public void ExitGame() {
        onGameExit?.Invoke();
    }

    public void NewPlayer() {
        onNewPlayer?.Invoke();
    }

    public void PlayerChosen() {
        onPlayerChosen?.Invoke();
    }

    public void Settings() {
        onSettings?.Invoke();
    }

    public void SettingsApply() {
        onSettingsApply?.Invoke();
    }

    public void SettingsCanceled() {
        onSettingsCanceled?.Invoke();
    }
}
