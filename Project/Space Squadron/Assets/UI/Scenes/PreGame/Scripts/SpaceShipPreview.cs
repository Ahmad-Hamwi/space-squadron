﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShipPreview : MonoBehaviour
{

    private float previewRotationSpeed;

    private float minSpeed;

    private float maxSpeed;

    private float currentSpeed;

    private float accelerationRate;

    private void OnEnable()
    {
        transform.rotation = Quaternion.Euler(new Vector3(-90, 0, 0));
    }

    void Start() {
        previewRotationSpeed = 20;
    }
    void Update() {
        transform.rotation *= Quaternion.Euler(0, 0, previewRotationSpeed * Time.deltaTime);
    }
}
