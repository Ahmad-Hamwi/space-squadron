﻿using System;
using System.Dynamic;
using System.Collections;
using System.Collections.Generic;
using Space_Squadron.Assets.Data.Entities;
using Space_Squadron.Assets.Data.Repositories.Settings;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using TMPro;

public class SettingsMenuManager : MonoBehaviour
{

    [SerializeField]
    private Button decreaseDisplayResolutionButton;

    [SerializeField]
    private Button increaseDisplayResolutionButton;

    private int ScreenResolutionChoice;

    [SerializeField]
    private TMP_Text DisplayResolutionText;

    private IList<string> screenResolutions;

    //////////////////////////////////////////////////////
    
    [SerializeField]
    private Button decreaseGraphicsQualityButton;

    [SerializeField]
    private Button increaseGraphicsQualityButton;

    private int graphicsQualityChoice;

    [SerializeField]
    private TMP_Text graphicsQualityText;

    private IList<string> graphicsQualities;

    //////////////////////////////////////////////////////
    
    [SerializeField]
    private Button decreaseControlMethodButton;

    [SerializeField]
    private Button increaseControlMethodButton;

    private int controlMethodChoice;

    [SerializeField]
    private TMP_Text controlMethodText;

    private IList<string> controlMethods;

    //////////////////////////////////////////////////////

    [SerializeField]
    private Button decreaseControl2MethodButton;

    [SerializeField]
    private Button increaseControl2MethodButton;

    private int control2MethodChoice;

    [SerializeField]
    private TMP_Text control2MethodText;

    //////////////////////////////////////////////////////

    [SerializeField]
    private Slider volumeSlider;

    private float volumeChoice;

    //////////////////////////////////////////////////////
    
    [SerializeField]
    private Button decreaseGameDifficultyButton;

    [SerializeField]
    private Button increaseGameDifficultyButton;
    
    private int gameDifficultyChoice;

    [SerializeField]
    private TMP_Text gameDifficultyText;

    private IList<string> difficulties;

    //////////////////////////////////////////////////////

    [SerializeField]
    private Button apply;

    public AudioMixer mixer;

    //////////////////////////////////////////////////////

    private SettingsRepository settingsRepository;

    private SettingsEntity settings;

    private void Awake() {
        settingsRepository = SettingsRepositoryImpl.GetInstance();

        screenResolutions = new List<string>();
        screenResolutions.Add("1920x1080");
        screenResolutions.Add("1280x720");

        graphicsQualities = new List<string>();
        graphicsQualities.Add("Low");
        graphicsQualities.Add("Medium");
        graphicsQualities.Add("High");

        controlMethods = new List<string>();
        controlMethods.Add("M&KB");
        controlMethods.Add("Gamepad");

        difficulties = new List<string>();
        difficulties.Add("Easy");
        difficulties.Add("Medium");
        difficulties.Add("Hard");
    }

    private void Start() {
        MenuEvents.instance.onSettingsApply += ApplySettings;
    }

    public void ApplySettings() {
        Debug.Log(controlMethodChoice);
        Debug.Log(control2MethodChoice);
        string[] res = screenResolutions[ScreenResolutionChoice].Split('x');
        int width = Convert.ToInt32(res[0]);
        int height = Convert.ToInt32(res[1]);
        SettingsEntity created = new SettingsEntity() {
            id = settings.id,
            displayResolutionWidth = width,
            displayResolutionHeight = height,
            graphicsQuality = graphicsQualityChoice,
            player1ControlType = controlMethodChoice,
            player2ControlType = control2MethodChoice,
            soundVolume = volumeSlider.value,
            gameDifficulty = gameDifficultyChoice
        };
        settingsRepository.ApplySettings(created);
        GameEvents.Instance().getOnControlsSelected()?.Invoke();
    }

    void OnEnable() {
        Debug.Log("settings enabled");
        settings = settingsRepository.GetCurrentPlayerSettings();

        string width = Convert.ToString(settings.displayResolutionWidth);
        string height = Convert.ToString(settings.displayResolutionHeight);
        string res = width + 'x' + height;

        for(int i = 0; i < screenResolutions.Count; i++) {
            if(res.Equals(screenResolutions[i])) {
                ScreenResolutionChoice = i;
            }
        }

        graphicsQualityChoice = settings.graphicsQuality;
        controlMethodChoice = settings.player1ControlType;
        control2MethodChoice = settings.player2ControlType;
        volumeChoice = settings.soundVolume;
        gameDifficultyChoice = settings.gameDifficulty;

        //Debug.Log(graphicsQualityChoice);
        Debug.Log(controlMethodChoice);
        Debug.Log(control2MethodChoice);
        //Debug.Log(volumeChoice);
        //Debug.Log(gameDifficultyChoice);
        
        DisplayResolutionText.text = screenResolutions[ScreenResolutionChoice];
        graphicsQualityText.text = graphicsQualities[graphicsQualityChoice];
        controlMethodText.text = controlMethods[controlMethodChoice];
        control2MethodText.text = controlMethods[control2MethodChoice];
        gameDifficultyText.text = difficulties[gameDifficultyChoice];
        volumeSlider.value = settings.soundVolume;

        apply.interactable = false;
    }

    private void OnDisable()
    {
        SettingsEntity settings = settingsRepository.GetCurrentPlayerSettings();
        settingsRepository.ApplySettings(settings);
        mixer.SetFloat("Volume", settings.soundVolume);
        
    }

    public void SliderOnChange(float value) {
        volumeChoice = value;
        mixer.SetFloat("Volume", value);
        if(CanBeApplied()) 
            apply.interactable = true;
        else 
            apply.interactable = false;
    }

    public void OnDecreaseScreenRes() {
        if(ScreenResolutionChoice == 0) {
            ScreenResolutionChoice = screenResolutions.Count - 1;
        } else {
            ScreenResolutionChoice--;
        }
        DisplayResolutionText.text = screenResolutions[ScreenResolutionChoice];
        if(CanBeApplied()) 
            apply.interactable = true;
        else 
            apply.interactable = false;
    }

    public void OnIncreaseScreenRes() {
        if(ScreenResolutionChoice == screenResolutions.Count - 1) {
            ScreenResolutionChoice = 0;
        } else {
            ScreenResolutionChoice++;
        }
        DisplayResolutionText.text = screenResolutions[ScreenResolutionChoice];
        if(CanBeApplied()) 
            apply.interactable = true;
        else 
            apply.interactable = false;
    }

    public void OnDecreaseGraphicsQuality() {
        
        if(graphicsQualityChoice == 0) {
            graphicsQualityChoice = graphicsQualities.Count - 1;
        } else {
            graphicsQualityChoice--;
        }
        graphicsQualityText.text = graphicsQualities[graphicsQualityChoice];
        if(CanBeApplied()) 
            apply.interactable = true;
        else 
            apply.interactable = false;
        Debug.Log(graphicsQualityChoice);
    }

    public void OnIncreaseGraphicsQuality() {
        
        if(graphicsQualityChoice == graphicsQualities.Count - 1) {
            graphicsQualityChoice = 0;
        } else {
            graphicsQualityChoice++;
        }
        graphicsQualityText.text = graphicsQualities[graphicsQualityChoice];
        if(CanBeApplied()) 
            apply.interactable = true;
        else 
            apply.interactable = false;
        Debug.Log(graphicsQualityChoice);
    }

    public void OnDecreaseControl() {
        Debug.Log(controlMethodChoice);
        if(controlMethodChoice == 0) {
            controlMethodChoice = controlMethods.Count - 1;
        } else {
            controlMethodChoice--;
        }
        controlMethodText.text = controlMethods[controlMethodChoice];
        if(CanBeApplied()) 
            apply.interactable = true;
        else 
            apply.interactable = false;
    }

    public void OnIncreaseControl() {
        Debug.Log(controlMethodChoice);
        if(controlMethodChoice == controlMethods.Count - 1) {
            controlMethodChoice = 0;
        } else {
            controlMethodChoice++;
        }
        controlMethodText.text = controlMethods[controlMethodChoice];
        if(CanBeApplied()) 
            apply.interactable = true;
        else 
            apply.interactable = false;
    }

    public void OnDecreaseControl2()
    {
        
        if (control2MethodChoice == 0)
        {
            control2MethodChoice = controlMethods.Count - 1;
        }
        else
        {
            control2MethodChoice--;
        }
        control2MethodText.text = controlMethods[control2MethodChoice];
        if (CanBeApplied())
            apply.interactable = true;
        else
            apply.interactable = false;

        Debug.Log(control2MethodChoice);
    }

    public void OnIncreaseControl2()
    {
        
        if (control2MethodChoice == controlMethods.Count - 1)
        {
            control2MethodChoice = 0;
        }
        else
        {
            control2MethodChoice++;
        }
        control2MethodText.text = controlMethods[control2MethodChoice];
        if (CanBeApplied())
            apply.interactable = true;
        else
            apply.interactable = false;
        Debug.Log(control2MethodChoice);
    }

    public void OnDecreaseDifficulty() {
        Debug.Log(gameDifficultyChoice);
        if(gameDifficultyChoice == 0) {
            gameDifficultyChoice = difficulties.Count - 1;
        } else {
            gameDifficultyChoice--;
        }
        gameDifficultyText.text = difficulties[gameDifficultyChoice];
        if(CanBeApplied()) 
            apply.interactable = true;
        else 
            apply.interactable = false;
    }

    public void OnIncreaseDifficulty() {
        Debug.Log(gameDifficultyChoice);
        if(gameDifficultyChoice == difficulties.Count - 1) {
            gameDifficultyChoice = 0;
        } else {
            gameDifficultyChoice++;
        }
        gameDifficultyText.text = difficulties[gameDifficultyChoice];
        if(CanBeApplied()) 
            apply.interactable = true;
        else 
            apply.interactable = false;
    }

    private bool CanBeApplied() => 
        controlMethodChoice != settings.player1ControlType ||
        control2MethodChoice != settings.player2ControlType ||
        gameDifficultyChoice != settings.gameDifficulty ||
        graphicsQualityChoice != settings.graphicsQuality ||
        volumeChoice != settings.soundVolume ||
        ScreenResolutionChoice != savedRes();

    private int savedRes() {
        string width = Convert.ToString(settings.displayResolutionWidth);
        string height = Convert.ToString(settings.displayResolutionHeight);
        string res = width + 'x' + height;

        for(int i = 0; i < screenResolutions.Count; i++) {
            if(res.Equals(screenResolutions[i])) {
                return i;
            }
        }
        return 0;
    }
    
}
