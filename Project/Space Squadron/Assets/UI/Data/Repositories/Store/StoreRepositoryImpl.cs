using System.Collections.Generic;
using Space_Squadron.Assets.Data.DataSources.PlayerProgress;
using Space_Squadron.Assets.Data.DataSources.Player;
using Space_Squadron.Assets.Data.DataSources.SpaceShip;
using Space_Squadron.Assets.Data.Entities;
using UnityEngine;
using Space_Squadron.Assets.Data.Constants;
using UnityEditor;

namespace Space_Squadron.Assets.Data.Repositories
{
    public class StoreRepositoryImpl : StoreRepository
    {
        private SpaceShipPersistentDataSource spaceShipPersistentDataSource;

        private SpaceShipCacheDataSource spaceShipCacheDataSource;

        private PlayerCacheDataSource playerCacheDataSource;

        private PlayerProgressPersistentDataSource playerProgressPersistentDataSource;

        private static StoreRepositoryImpl instance;

        private StoreRepositoryImpl() {
            spaceShipPersistentDataSource = SpaceShipPersistentDataSourceImpl.GetInstance();
            playerCacheDataSource = PlayerCacheDataSourceImpl.GetInstance();
            spaceShipCacheDataSource = SpaceShipCacheDataSourceImpl.GetInstance();
            playerProgressPersistentDataSource = PlayerProgressPersistentDataSourceImpl.GetInstance();
        }

        public static StoreRepositoryImpl GetInstance() {
            if(instance == null) {
                instance = new StoreRepositoryImpl();
            }
            return instance;
        }
        public IList<SpaceShipEntity> LoadPlayerSpaceShips()
        {
            string playerId = playerCacheDataSource.GetCachedPlayerId();
            return spaceShipPersistentDataSource.GetAllPlayerSpaceShips(playerId);
        }

        public void SelectSpaceShip(string spaceShipId)
        {
            string playerId = playerCacheDataSource.GetCachedPlayerId();
            spaceShipCacheDataSource.CacheSelectedSpaceShip(spaceShipId);
            spaceShipPersistentDataSource.SelectSpaceShip(playerId, spaceShipId);
        }

        public void PurchaseNewSpaceShip(string spaceShipId, int cost)
        {
            string playerId = playerCacheDataSource.GetCachedPlayerId();
            playerProgressPersistentDataSource.EditCoins(playerId, cost);
            spaceShipPersistentDataSource.PurchaseNewSpaceShip(playerId, spaceShipId);
        }

        public void SelectCamoOnSpaceShip(string spaceShipId, int camoIndex)
        {
            spaceShipPersistentDataSource.selectCamoOnSpaceShip(spaceShipId, camoIndex);
        }

        public int GetPlayerCoins()
        {
            string playerId = playerCacheDataSource.GetCachedPlayerId();
            return playerProgressPersistentDataSource.GetPlayerProgress(playerId).currentCoins;
        }
        public void SaveSecondPlayerSelection(int spaceShipIndex, int camoIndex)
        {
            PlayerPrefs.SetInt(PlayerPrefsKeys.CURRENT_PLAYER2_SPACESHIP_INDEX, spaceShipIndex);
            PlayerPrefs.SetInt(PlayerPrefsKeys.CURRENT_PLAYER2_CAMO_INDEX, camoIndex);
        }

        public void LevelUpSpaceShip(string spaceShipId, int cost)
        {
            string playerId = playerCacheDataSource.GetCachedPlayerId();
            playerProgressPersistentDataSource.EditCoins(playerId, cost);
            spaceShipPersistentDataSource.LevelUpSpaceShip(spaceShipId);
        }
    }
}