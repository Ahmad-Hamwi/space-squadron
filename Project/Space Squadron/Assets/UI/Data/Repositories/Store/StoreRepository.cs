using System;
using System.Collections.Generic;
using Space_Squadron.Assets.Data.Entities;

namespace Space_Squadron.Assets.Data.Repositories
{
    public interface StoreRepository
    {
        IList<SpaceShipEntity> LoadPlayerSpaceShips();

        void SelectSpaceShip(string spaceShipId);

        void PurchaseNewSpaceShip(string spaceShipId, int cost);

        void SelectCamoOnSpaceShip(string spaceShipId, int camoIndex);

        int GetPlayerCoins();
        void SaveSecondPlayerSelection(int spaceShipIndex, int camoIndex);
        void LevelUpSpaceShip(string spaceShipId, int cost);
    }
}