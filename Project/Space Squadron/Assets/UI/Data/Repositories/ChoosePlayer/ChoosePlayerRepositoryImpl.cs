using Space_Squadron.Assets.Data.DataSources;
using Space_Squadron.Assets.Data.DataSources.Player;
using Space_Squadron.Assets.Data.DataSources.SpaceShip;
using Space_Squadron.Assets.Data.Entities.ProgressData;
using System.Collections.Generic;


namespace Space_Squadron.Assets.Data.Repositories
{
    public class ChoosePlayerRepositoryImpl : ChoosePlayerRepository
    {

        private static ChoosePlayerRepositoryImpl instance;

        private PlayerPersistentDataSource playerPersistentDataSource;
        private PlayerCacheDataSource playerCacheDataSource;

        private ChoosePlayerRepositoryImpl() {
            this.playerPersistentDataSource = PlayerPersistentDataSourceImpl.GetInstance();
            this.playerCacheDataSource = PlayerCacheDataSourceImpl.GetInstance();
        }

        public static ChoosePlayerRepositoryImpl GetInstance() {
            if(instance == null) {
                instance = new ChoosePlayerRepositoryImpl();
            }
            return instance;
        }

        public void CreateAndLoadGuestPlayer() {
            CacheSelectedPlayer(playerPersistentDataSource.CreateNewPlayerFile("Guest", true));
        }

        public void CreateAndLoadNewPlayer(string playerName) {
            CacheSelectedPlayer(playerPersistentDataSource.CreateNewPlayerFile(playerName, false));
        }

        public IList<PlayerEntity> ShowCreatedPlayers() {
            return playerPersistentDataSource.GetAllPlayers();
        }

        public void LoadSelectedPlayer(string playerId) {
            CacheSelectedPlayer(playerPersistentDataSource.LoadPlayer(playerId));
        }

        private void CacheSelectedPlayer(PlayerEntity player) {
            playerCacheDataSource.CacheCurrentPlayerId(player.id);
            playerCacheDataSource.CacheCurrentPlayerName(player.name);
            playerCacheDataSource.CacheCurrentPlayerAsGuest(player.isGuest);
        }

        public void DeleteUser(PlayerEntity playerEntity)
        {
            playerPersistentDataSource.DeleteUser(playerEntity.id);
        }
    }
}