using System.Runtime;
using Assets.ViewData;
using Space_Squadron.Assets.Data.Entities;
using Space_Squadron.Assets.Data.Entities.GameData;

namespace Space_Squadron.Assets.Data.Repositories
{
    public interface InGameRepository
    {
        SpaceShipEntity GetCurrentPlayerSpaceShip();

        SceneEntity GetCurrentScene();

        PlayerMode GetPlayerMode();

        int GetPlayerControlType();

        int GetPlayer2ControlType();

        void LevelCompleted(PlayerProgressEntity currentLevelProgress);

        SpaceShipEntity GetPlayer2SpaceShip();
    }
}