using Space_Squadron.Assets.Data.Entities;
using Space_Squadron.Assets.Data.DataSources.SpaceShip;
using Space_Squadron.Assets.Data.Entities.GameData;
using Space_Squadron.Assets.Data.DataSources.Scene;
using Space_Squadron.Assets.Data.DataSources.Player;
using Space_Squadron.Assets.Data.DataSources.Settings;
using Assets.ViewData;
using UnityEngine;
using Space_Squadron.Assets.Data.Constants;

namespace Space_Squadron.Assets.Data.Repositories
{
    public class InGameRepositoryImpl : InGameRepository
    {
        private static InGameRepositoryImpl instance;

        private SpaceShipCacheDataSource spaceShipCacheDataSource;
        private SpaceShipPersistentDataSource spaceShipPersistentDataSource;
        private ScenePersistentDataSource scenePersistentDataSource;
        private SceneCacheDataSource sceneCacheDataSource;
        private SettingsCacheDataSource settingsCacheDataSource;

        private PlayerCacheDataSource playerCacheDataSource;
        private InGameRepositoryImpl() {
            spaceShipCacheDataSource = SpaceShipCacheDataSourceImpl.GetInstance();
            spaceShipPersistentDataSource = SpaceShipPersistentDataSourceImpl.GetInstance();
            scenePersistentDataSource = ScenePersistentDataSourceImpl.GetInstance();
            sceneCacheDataSource = SceneCacheDataSourceImpl.GetInstance();
            settingsCacheDataSource = SettingsCacheDataSourceImpl.GetInstance();
            playerCacheDataSource = PlayerCacheDataSourceImpl.GetInstance();
        }

        public static InGameRepositoryImpl GetInstance() {
            if(instance == null) {
                return new InGameRepositoryImpl();
            } else {
                return instance;
            }
        }
        public SpaceShipEntity GetCurrentPlayerSpaceShip()
        {
            string currentPlayerId = playerCacheDataSource.GetCachedPlayerId();
            return spaceShipPersistentDataSource.GetCurrentSelectedSpaceShip(currentPlayerId);
        }

        public SceneEntity GetCurrentScene()
        {
            int sceneIndex = sceneCacheDataSource.GetCurrentSceneIndex();
            int sceneDifficulty = settingsCacheDataSource.GetCachedDifficulty();
            return scenePersistentDataSource.LoadScene(sceneIndex, sceneDifficulty);
        }

        public int GetPlayerControlType()
        {
            return settingsCacheDataSource.GetCachedControlType();
        }

        public int GetPlayer2ControlType()
        {
            return settingsCacheDataSource.GetCachedControl2Type();
        }

        public PlayerMode GetPlayerMode()
        {
            int playersCount = 0;
            playersCount = PlayerPrefs.GetInt(PlayerPrefsKeys.CURRENT_PLAYER_COUNT, 1);
            if(playersCount == 1)
            {
                return PlayerMode.SINGLE_PLAYER;
            }
            else
            {
                return PlayerMode.MULTIPLAYER;
            }
        }

        public void LevelCompleted(PlayerProgressEntity currentLevelProgress)
        {
            string playerId = playerCacheDataSource.GetCachedPlayerId();
            int botsEliminatedCount = currentLevelProgress.enemiesEliminated;
            int currentLevelIndex = currentLevelProgress.lastUnlockedLevelIndex;
            int coinsReward = currentLevelProgress.currentCoins;
            int currentLevelScore = currentLevelProgress.totalGameScore;
        }

        public SpaceShipEntity GetPlayer2SpaceShip()
        {
            string currentPlayerId = PlayerPrefs.GetString(PlayerPrefsKeys.CURRENT_PLAYER_ID, "error");
            int index = PlayerPrefs.GetInt(PlayerPrefsKeys.CURRENT_PLAYER2_SPACESHIP_INDEX, 0);
            return spaceShipPersistentDataSource.GetCurrentPlayer2SelectedSpaceShip(currentPlayerId, index);
        }
    }
}