using Space_Squadron.Assets.Data.Entities.ProgressData;
using System.Collections.Generic;

namespace Space_Squadron.Assets.Data.DataSources
{
    public interface PlayerPersistentDataSource
    {
        PlayerEntity CreateNewPlayerFile(string playerName, bool isGuest);
        IList<PlayerEntity> GetAllPlayers();
        PlayerEntity LoadPlayer(string playerId);
        void DeleteUser(string id);
    }
}