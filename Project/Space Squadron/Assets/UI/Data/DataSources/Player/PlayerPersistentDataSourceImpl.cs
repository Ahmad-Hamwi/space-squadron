using System.Collections.Generic;
using Space_Squadron.Assets.Data.Entities.ProgressData;
using Space_Squadron.Assets.Data.FileManagers;

namespace Space_Squadron.Assets.Data.DataSources.Player
{
    public class PlayerPersistentDataSourceImpl : PlayerPersistentDataSource
    {

        private static PlayerPersistentDataSourceImpl instance;
        private PlayerFileManager fileManager;

        private PlayerPersistentDataSourceImpl() {
            fileManager = PlayerFileManagerImpl.GetInstance();
        }

        public static PlayerPersistentDataSourceImpl GetInstance() {
            if(instance == null) {
                instance = new PlayerPersistentDataSourceImpl();
            }
            return instance;
        }

        public PlayerEntity CreateNewPlayerFile(string playerName, bool isGuest)
        {
            return fileManager.CreateNewPlayerFile(playerName, isGuest);
        }

        public void DeleteUser(string id)
        {
            fileManager.Delete(id);
        }

        public IList<PlayerEntity> GetAllPlayers()
        {
            return fileManager.GetAllPlayers();
        }

        public PlayerEntity LoadPlayer(string playerId)
        {
            return fileManager.FindPlayerById(playerId);
        }
    }
}