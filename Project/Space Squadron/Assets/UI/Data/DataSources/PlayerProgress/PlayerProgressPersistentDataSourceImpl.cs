using Space_Squadron.Assets.Data.Entities;
using Space_Squadron.Assets.Data.FileManagers.PlayerProgress;

namespace Space_Squadron.Assets.Data.DataSources.PlayerProgress
{
    public class PlayerProgressPersistentDataSourceImpl : PlayerProgressPersistentDataSource
    {

        private PlayerProgressFileManager fileManager;

        private static PlayerProgressPersistentDataSourceImpl instance;

        private PlayerProgressPersistentDataSourceImpl() {
            fileManager = PlayerProgressFileManagerImpl.GetInstance();
        }

        public static PlayerProgressPersistentDataSourceImpl GetInstance() {
            if(instance == null) {
                instance = new PlayerProgressPersistentDataSourceImpl();
            }
            return instance;
        }

        public void EditCoins(string playerId, int cost)
        {
            PlayerProgressEntity extracted = fileManager.ReadFile(playerId);
            extracted.currentCoins -= cost;
            fileManager.WriteFile(extracted);
        }

        public PlayerProgressEntity GetPlayerProgress(string playerId)
        {
            return fileManager.ReadFile(playerId);
        }
    }
}