using System.Globalization;
using System.Runtime.CompilerServices;
using System.ComponentModel;
using Space_Squadron.Assets.Data.Entities;
using System.Collections.Generic;
using Space_Squadron.Assets.Data.FileManagers.PlayerSpaceShip;
using System.IO;

namespace Space_Squadron.Assets.Data.DataSources.SpaceShip
{
    public class SpaceShipPersistentDataSourceImpl : SpaceShipPersistentDataSource
    {

        private SpaceShipFileManager spaceShipFileManager;

        private static SpaceShipPersistentDataSourceImpl instance;

        private SpaceShipPersistentDataSourceImpl() {
            spaceShipFileManager = SpaceShipFileManagerImpl.GetInstance();
        }

        public static SpaceShipPersistentDataSourceImpl GetInstance() {
            if(instance == null) {
                instance =  new SpaceShipPersistentDataSourceImpl();
            }
            return instance;
        }

        public IList<SpaceShipEntity> GetAllPlayerSpaceShips(string playerId)
        {
            return spaceShipFileManager.GetPlayerSpaceShips(playerId);
        }

        public void PurchaseNewSpaceShip(string currentPlayerId, string spaceShipId)
        {
            ResetSelection(currentPlayerId);
            SpaceShipEntity extracted = spaceShipFileManager.ReadFile(spaceShipId);
            extracted.isSelected = true;
            extracted.isUnlocked = true;
            spaceShipFileManager.WriteFile(extracted);
        }

        public SpaceShipEntity GetCurrentSelectedSpaceShip(string currentPlayerId)
        {
            IList<SpaceShipEntity> playerSpaceShips = spaceShipFileManager.GetPlayerSpaceShips(currentPlayerId);
            foreach(SpaceShipEntity spaceShip in playerSpaceShips) {
                if(spaceShip.isSelected) return spaceShip;
            }
            return null;
        }

        public void selectCamoOnSpaceShip(string spaceShipId, int camoIndex)
        {
            SpaceShipEntity extracted = spaceShipFileManager.ReadFile(spaceShipId);
            extracted.camoIndex = camoIndex;
            spaceShipFileManager.WriteFile(extracted);
        }

        public void SelectSpaceShip(string currentPlayerId, string spaceShipId)
        {
            ResetSelection(currentPlayerId);
            SpaceShipEntity extracted = spaceShipFileManager.ReadFile(spaceShipId);
            extracted.isSelected = true;
            spaceShipFileManager.WriteFile(extracted);
        }

        private void ResetSelection(string currentPlayerId)
        {
            IList<SpaceShipEntity> playerSpaceShips = spaceShipFileManager.GetPlayerSpaceShips(currentPlayerId);
            foreach (SpaceShipEntity spaceShip in playerSpaceShips)
            {
                spaceShip.isSelected = false;
                spaceShipFileManager.WriteFile(spaceShip);
            }
        }

        public SpaceShipEntity GetCurrentPlayer2SelectedSpaceShip(string currentPlayerId, int index)
        {
            IList<SpaceShipEntity> playerSpaceShips = spaceShipFileManager.GetPlayerSpaceShips(currentPlayerId);
            foreach (SpaceShipEntity spaceShip in playerSpaceShips)
            {
                if (spaceShip.index == index) return spaceShip;
            }
            return null;
        }

        public void LevelUpSpaceShip(string spaceShipId)
        {
            SpaceShipEntity extracted = spaceShipFileManager.ReadFile(spaceShipId);
            extracted.hp += 30;
            extracted.level++;
            spaceShipFileManager.WriteFile(extracted);
        }
    }
}