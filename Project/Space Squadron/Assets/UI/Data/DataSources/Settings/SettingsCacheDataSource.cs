using Space_Squadron.Assets.Data.Entities;

namespace Space_Squadron.Assets.Data.DataSources.Settings
{
    public interface SettingsCacheDataSource
    {
        void ApplyInGameComponents(SettingsEntity settings);
        void CachePrefs(SettingsEntity settings);
        int GetCachedDifficulty();
        int GetCachedControlType();
        int GetCachedControl2Type();
    }
}