using Space_Squadron.Assets.Data.Constants;
using Space_Squadron.Assets.Data.Entities;
using UnityEngine;
using UnityEngine.UI;

namespace Space_Squadron.Assets.Data.DataSources.Settings
{
    public class SettingsCacheDataSourceImpl : SettingsCacheDataSource
    {

        private static SettingsCacheDataSourceImpl instance;

        private SettingsCacheDataSourceImpl() {
        }

        public static SettingsCacheDataSourceImpl GetInstance() {
            if(instance == null) {
                instance = new SettingsCacheDataSourceImpl();
            }
            return instance;
        }
        public void ApplyInGameComponents(SettingsEntity settings)
        {
            Screen.SetResolution(settings.displayResolutionWidth, settings.displayResolutionHeight, true);
            QualitySettings.SetQualityLevel(settings.graphicsQuality);
        }

        public void CachePrefs(SettingsEntity settings)
        {
            PlayerPrefs.SetInt(PlayerPrefsKeys.CURRENT_CONTROL_TYPE, settings.player1ControlType);
            PlayerPrefs.SetInt(PlayerPrefsKeys.CURRENT_CONTROL2_TYPE, settings.player2ControlType);
            PlayerPrefs.SetInt(PlayerPrefsKeys.CURRENT_DIFFICULTY_CHOICE, settings.gameDifficulty);
        }

        public int GetCachedControl2Type()
        {
            return PlayerPrefs.GetInt(PlayerPrefsKeys.CURRENT_CONTROL_TYPE, 0);
        }

        public int GetCachedControlType()
        {
            return PlayerPrefs.GetInt(PlayerPrefsKeys.CURRENT_CONTROL2_TYPE, 0);
        }

        public int GetCachedDifficulty()
        {
            return PlayerPrefs.GetInt(PlayerPrefsKeys.CURRENT_DIFFICULTY_CHOICE, 0);
        }
    }
}