using Space_Squadron.Assets.Data.Entities;

namespace Space_Squadron.Assets.Data.DataSources.Settings
{
    public interface SettingsPersistentDataSource
    {
        SettingsEntity GetPlayerSettings(string currentPlayerId);
        void SavePlayerSettings(SettingsEntity settings);
    }
}