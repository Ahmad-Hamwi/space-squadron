using Space_Squadron.Assets.Data.Entities.GameData;
using Space_Squadron.Assets.Data.FileManagers.Scene;

namespace Space_Squadron.Assets.Data.DataSources.Scene
{
    public class ScenePersistentDataSourceImpl : ScenePersistentDataSource
    {

        private static ScenePersistentDataSourceImpl instance;

        private SceneFileManager fileManager;

        private ScenePersistentDataSourceImpl() {
            fileManager = SceneFileManagerImpl.GetInstance();
        }

        public static ScenePersistentDataSourceImpl GetInstance() {
            if(instance == null) {
                instance = new ScenePersistentDataSourceImpl();
            }
            return instance;
        }
        public SceneEntity LoadScene(int sceneIndex, int sceneDifficulty)
        {
            return fileManager.ReadFile(sceneIndex, sceneDifficulty);
        }
    }
}