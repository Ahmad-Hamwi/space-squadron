using System;
namespace Space_Squadron.Assets.Data.Entities
{
    [SerializableAttribute]
    public class SettingsEntity
    {
        public string id { get; set; }

        public int displayResolutionWidth { get; set; }

        public int displayResolutionHeight { get; set; }

        public int graphicsQuality { get; set; }

        public int player1ControlType { get; set; }

        
        public int player2ControlType { get; set; }

        public float soundVolume { get; set; }

        public int gameDifficulty { get; set; }
    }

}