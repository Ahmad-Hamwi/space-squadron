using System;

namespace Space_Squadron.Assets.Data.Entities.GameData
{
    [SerializableAttribute]
    public class WeaponEntity
    {
        public BulletEntity bullet { get; set; }
        public float timeGap { get; set; }
        public float fireRate { get; set; }
    }
}