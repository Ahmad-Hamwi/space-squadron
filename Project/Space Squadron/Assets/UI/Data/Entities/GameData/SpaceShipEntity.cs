using System;
using System.Collections;
using System.Collections.Generic;
using Assets.UI.Data.Entities.GameData;
using Space_Squadron.Assets.Data.Entities.GameData;

namespace Space_Squadron.Assets.Data.Entities
{
    [SerializableAttribute]
    public class SpaceShipEntity
    {
        public string id { get; set; }
        public string playerId { get; set; }
        public bool isUnlocked { get; set; }
        public bool isSelected { get; set; }
        public int cost { get; set; }
        public int level { get; set; }
        public float minSpeed { get; set; }
        public float maxSpeed { get; set; }
        public float sprintSpeed { get; set; }
        public float hp { get; set; }
        public float healthRegen { get; set; }
        public float scoreValue { get; set; }
        public float shield { get; set; }
        public List<ItemEntity> itemsToDrop { get; set; }
        public int availableSlots { get; set; }
        public int index { get; set; }
        public int price { get; set; }
        public IList<int> levelPrices { get; set; }
        public int camoIndex { get; set; }
        public WeaponEntity primaryWeaponLevel1 { get; set; }
        public WeaponEntity primaryWeaponLevel2 { get; set; }
        public WeaponEntity primaryWeaponLevel3 { get; set; }
        public WeaponEntity secondaryWeapon { get; set; }
    }
    
}