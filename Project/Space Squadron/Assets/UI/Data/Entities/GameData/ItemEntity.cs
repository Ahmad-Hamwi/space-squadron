﻿using Space_Squadron.Assets.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.UI.Data.Entities.GameData
{
    [Serializable]
    public class ItemEntity
    {
        private ItemType type;
        private float value { get; set; }
    }

    [Flags]
    public enum ItemType : byte
    {
        NO_ITEM = 0, HEALING = 1, SHIELD = 2, SCORE = 3, SPECIAL = 4
    }
}
