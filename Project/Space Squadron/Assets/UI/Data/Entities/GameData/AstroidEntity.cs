using System;

namespace Space_Squadron.Assets.Data.Entities.GameData
{
    [SerializableAttribute]
    public class AstroidEntity
    {
        public float scoreValue { get; set; }
        public float hp { get; set; }
        public int level { get; set; }
    }
}