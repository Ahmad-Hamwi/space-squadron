using System;

namespace Space_Squadron.Assets.Data.Util
{
    public class TimeBasedIdGenerator
    {
        public static string GenerateId() {
            long ticks = DateTime.Now.Ticks;
            byte[] bytes = BitConverter.GetBytes(ticks);
            string id = Convert.ToBase64String(bytes)
                        .Replace('+', '_')
                        .Replace('/', '-')
                        .TrimEnd('=');
                        return id;
        }
    }
}