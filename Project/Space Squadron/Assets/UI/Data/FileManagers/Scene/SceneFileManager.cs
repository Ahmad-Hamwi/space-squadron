using Space_Squadron.Assets.Data.Entities.GameData;

namespace Space_Squadron.Assets.Data.FileManagers.Scene
{
    public interface SceneFileManager
    {
        SceneEntity ReadFile(int sceneIndex, int level);
        void WriteFile(SceneEntity extracted);
    }
}