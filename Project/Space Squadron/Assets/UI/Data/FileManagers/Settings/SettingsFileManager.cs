using Space_Squadron.Assets.Data.Entities;

namespace Space_Squadron.Assets.Data.FileManagers.Settings
{
    public interface SettingsFileManager
    {
        SettingsEntity FindSettingsById(string playerId);

        void SavePlayerSettings(SettingsEntity settings);
    }
}