using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Assets.UI.Data.Constants;
using Space_Squadron.Assets.Data.Constants;
using Space_Squadron.Assets.Data.DataSources.SpaceShip;
using Space_Squadron.Assets.Data.Entities;
using Space_Squadron.Assets.Data.Entities.GameData;
using Space_Squadron.Assets.Data.Util;
using TMPro;
using UnityEngine;
using System;
using Assets.UI.Data.Entities.GameData;

namespace Space_Squadron.Assets.Data.FileManagers.PlayerSpaceShip
{
    public class SpaceShipFileManagerImpl : SpaceShipFileManager
    {
        private static SpaceShipFileManagerImpl instance;
        private PlayerSpaceShipGenerator spaceShipGenerator;

        private SpaceShipFileManagerImpl() {
            spaceShipGenerator = new PlayerSpaceShipGenerator();
        }

        public static SpaceShipFileManagerImpl GetInstance() {
            if(instance == null) {
                instance = new SpaceShipFileManagerImpl();
            }
            return instance;
        }

        public IList<SpaceShipEntity> GetPlayerSpaceShips(string playerId)
        {
            Directory.CreateDirectory(Application.persistentDataPath + FileEndPoints.PLAYER_SPACESHIP_PATH);
            string[] saveFiles = Directory.GetFiles(Application.persistentDataPath + FileEndPoints.PLAYER_SPACESHIP_PATH);
            BinaryFormatter formatter = new BinaryFormatter();
            List<SpaceShipEntity> spaceShips = new List<SpaceShipEntity>();
            foreach (string fileDirectory in saveFiles) {
                using(FileStream fileStream = new FileStream(fileDirectory, FileMode.Open)) {
                    SpaceShipEntity spaceShip = (SpaceShipEntity)formatter.Deserialize(fileStream);
                    if(spaceShip.playerId == playerId) spaceShips.Add(spaceShip);
                }
            }
            spaceShips.Sort((x, y) => x.index - y.index);
            if(spaceShips.Count < 4) {
                IList<bool> spaceShipFileExist = new List<bool>();
                for (int i = 0; i < GameConstants.MAX_PLAYER_SPACESHIPS; i++) spaceShipFileExist.Add(false);
                foreach(SpaceShipEntity spaceShip in spaceShips) {
                    spaceShipFileExist[spaceShip.index] = true;
                }
                for(int i = 0; i < GameConstants.MAX_PLAYER_SPACESHIPS; i++) {
                    if(!spaceShipFileExist[i]) {
                        SpaceShipEntity generatedSpaceShip = null;
                        switch(i)
                        {
                            case 0:
                                generatedSpaceShip = spaceShipGenerator.GenerateDefaultSpaceShip1(playerId);
                                break;
                            case 1:
                                generatedSpaceShip = spaceShipGenerator.GenerateDefaultSpaceShip2(playerId);
                                break;
                            case 2:
                                generatedSpaceShip = spaceShipGenerator.GenerateDefaultSpaceShip3(playerId);
                                break;
                            case 3:
                                generatedSpaceShip = spaceShipGenerator.GenerateDefaultSpaceShip4(playerId);
                                break;
                        }
                        WriteFile(generatedSpaceShip); 
                        continue;
                    }
                }
                return GetPlayerSpaceShips(playerId);
            } else {
                bool isOneSelected = false;
                bool isOneUnlocked = false;
                foreach(SpaceShipEntity spaceShip in spaceShips) {
                    if(spaceShip.isSelected) isOneSelected = true;
                    if (spaceShip.isUnlocked) isOneUnlocked = true;
                }
                SpaceShipEntity firstSpaceShip = null;
                firstSpaceShip = spaceShips[0];
                if(!isOneSelected) {
                    firstSpaceShip.isSelected = true;
                }
                if(!isOneUnlocked) {
                    firstSpaceShip.isUnlocked = true;
                }
                WriteFile(firstSpaceShip);
                return spaceShips;
            }
        }

        public SpaceShipEntity ReadFile(string spaceShipId)
        {
            string path = Application.persistentDataPath + FileEndPoints.PLAYER_SPACESHIP_PATH + spaceShipId + ".save";
            SpaceShipEntity extracted = null;
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fileStream = new FileStream(path, FileMode.Open))
            {
                extracted = (SpaceShipEntity)formatter.Deserialize(fileStream);
            }
            return extracted;
        }

        public void WriteFile(SpaceShipEntity extracted)
        {
            string path = Application.persistentDataPath + FileEndPoints.PLAYER_SPACESHIP_PATH;
            Directory.CreateDirectory(path);
            BinaryFormatter formatter = new BinaryFormatter();
            using (FileStream fileStream = new FileStream(path + extracted.id + ".save", FileMode.Create))
            {
                formatter.Serialize(fileStream, extracted);
            }
        }

        //public void modifySelectedSpaceShipCamo(string spaceShipId, int camoIndex)
        //{
        //    string path = Application.persistentDataPath + FileEndPoints.PLAYER_SPACESHIP_PATH;
        //    Directory.CreateDirectory(path);
        //    string[] saveFiles = Directory.GetFiles(path);
        //    BinaryFormatter formatter = new BinaryFormatter();
        //    SpaceShipEntity extracted = null;

        //    foreach (string fileDirectory in saveFiles)
        //    {
        //        if (fileDirectory.Equals(path + spaceShipId + ".save"))
        //        {
        //            using (FileStream read = new FileStream(fileDirectory, FileMode.Open))
        //            {
        //                extracted = (SpaceShipEntity)formatter.Deserialize(read);
        //            }
        //        }
        //    }

        //    extracted.isUnlocked = true;

        //    using (FileStream write = new FileStream(path + extracted.id + ".save", FileMode.Create))
        //    {
        //        formatter.Serialize(write, extracted);
        //    }

        //    foreach (string fileDirectory in saveFiles)
        //    {
        //        using (FileStream read = new FileStream(fileDirectory, FileMode.Open))
        //        {
        //            SpaceShipEntity spaceShip = (SpaceShipEntity)formatter.Deserialize(read);
        //            if (spaceShip.id == spaceShipId)
        //            {
        //                extracted = spaceShip;
        //                break;
        //            }
        //        }
        //    }

        //    extracted.isUnlocked = true;

        //    using (FileStream write = new FileStream(path + extracted.id + ".save", FileMode.Create))
        //    {
        //        formatter.Serialize(write, extracted);
        //    }
        //}

        // public SpaceShipEntity GetSpaceShip(string spaceShipId)
        // {
        //     string[] spaceShipSaveFiles = Directory.GetFiles(Application.persistentDataPath + FileEndPoints.PLAYER_SPACESHIP_PATH);
        //     BinaryFormatter formatter = new BinaryFormatter();
        //     foreach (string fileDirectory in spaceShipSaveFiles) {
        //         using(FileStream fileStream = new FileStream(fileDirectory, FileMode.Open)) {
        //             SpaceShipEntity spaceShip = (SpaceShipEntity)formatter.Deserialize(fileStream);
        //             if(spaceShip.id == spaceShipId) return spaceShip;
        //         }
        //     }
        //     return null;
        // }

        //public void UnlockSpaceShip(string spaceShipId)
        //{
        //    string path = Application.persistentDataPath + FileEndPoints.PLAYER_SPACESHIP_PATH;
        //    Directory.CreateDirectory(path);
        //    string[] saveFiles = Directory.GetFiles(path);
        //    BinaryFormatter formatter = new BinaryFormatter();
        //    SpaceShipEntity extracted = null;

        //    foreach (string fileDirectory in saveFiles) {
        //        using(FileStream read = new FileStream(fileDirectory, FileMode.Open)) {
        //            SpaceShipEntity spaceShip = (SpaceShipEntity)formatter.Deserialize(read);
        //            if(spaceShip.id == spaceShipId) 
        //            {
        //                extracted = spaceShip;
        //                break;
        //            } 
        //        }
        //    }

        //    extracted.isUnlocked = true;

        //    using(FileStream write = new FileStream(path + extracted.id + ".save", FileMode.Create)) {
        //        formatter.Serialize(write, extracted);
        //    }
        //}

        private class PlayerSpaceShipGenerator {

            public SpaceShipEntity GenerateDefaultSpaceShip(string playerId, int index) {
                return new SpaceShipEntity() {playerId = playerId, id = TimeBasedIdGenerator.GenerateId(), level = 1, hp = 300, healthRegen = 5, shield = 30, minSpeed = 20, maxSpeed = 50, sprintSpeed = 150,
                    primaryWeaponLevel1 = new WeaponEntity() { fireRate = 0.45f, bullet = new BulletEntity() { damage = 25, speed = 200, type = BulletType.FORWARD } },
                    primaryWeaponLevel2 = new WeaponEntity() { fireRate = 0.45f, bullet = new BulletEntity() { damage = 25, speed = 200, type = BulletType.FORWARD } },
                    primaryWeaponLevel3 = new WeaponEntity() { fireRate = 0.45f, bullet = new BulletEntity() { damage = 25, speed = 200, type = BulletType.FORWARD } },
                    isUnlocked = false, index = index, levelPrices = new List<int>() { 1000, 3500, 6500 } };
            }

            public SpaceShipEntity GenerateDefaultSpaceShip1(string playerId)
            {
                return new SpaceShipEntity()
                {
                    playerId = playerId,
                    id = TimeBasedIdGenerator.GenerateId(),
                    level = 1,
                    hp = 300,
                    healthRegen = 5,
                    shield = 30,
                    minSpeed = 20,
                    maxSpeed = 50,
                    sprintSpeed = 150,
                    primaryWeaponLevel1 = new WeaponEntity() { timeGap = 0.1f, bullet = new BulletEntity() { damage = 5, speed = 1000, type = BulletType.FORWARD } },
                    primaryWeaponLevel2 = new WeaponEntity() { timeGap = 0.1f, bullet = new BulletEntity() { damage = 5, speed = 1000, type = BulletType.FORWARD } },
                    primaryWeaponLevel3 = new WeaponEntity() { timeGap = 0.1f, bullet = new BulletEntity() { damage = 5, speed = 1000, type = BulletType.FORWARD } },
                    secondaryWeapon = new WeaponEntity() { timeGap = 0.5f, bullet = new BulletEntity() {damage = 25, speed = 1000, type = BulletType.FOLLOW}},
                    isUnlocked = false,
                    index = 0,
                    levelPrices = new List<int>() { 1000, 3500, 6500 },
                    price = 0
                };
            }

            public SpaceShipEntity GenerateDefaultSpaceShip2(string playerId)
            {
                return new SpaceShipEntity()
                {
                    playerId = playerId,
                    id = TimeBasedIdGenerator.GenerateId(),
                    level = 1,
                    hp = 350,
                    healthRegen = 5,
                    shield = 35,
                    minSpeed = 20,
                    maxSpeed = 50,
                    sprintSpeed = 150,
                    primaryWeaponLevel1 = new WeaponEntity() { timeGap = 0.1f, bullet = new BulletEntity() { damage = 5, speed = 1000, type = BulletType.FORWARD } },
                    primaryWeaponLevel2 = new WeaponEntity() { timeGap = 0.1f, bullet = new BulletEntity() { damage = 5, speed = 1000, type = BulletType.FORWARD } },
                    primaryWeaponLevel3 = new WeaponEntity() { bullet = new BulletEntity() { damage = 20, type = BulletType.LASER } },
                    secondaryWeapon = new WeaponEntity() { timeGap = 0.5f, bullet = new BulletEntity() {damage = 25, speed = 1000, type = BulletType.FOLLOW}},
                    isUnlocked = false,
                    index = 1,
                    levelPrices = new List<int>() { 1000, 3500, 6500 },
                    price = 3500
                };
            }

            public SpaceShipEntity GenerateDefaultSpaceShip3(string playerId)
            {
                return new SpaceShipEntity()
                {
                    playerId = playerId,
                    id = TimeBasedIdGenerator.GenerateId(),
                    level = 1,
                    hp = 400,
                    healthRegen = 5,
                    shield = 40,
                    minSpeed = 20,
                    maxSpeed = 50,
                    sprintSpeed = 150,
                    primaryWeaponLevel1 = new WeaponEntity() { bullet = new BulletEntity() { damage = 20, type = BulletType.LASER } },
                    primaryWeaponLevel2 = new WeaponEntity() { bullet = new BulletEntity() { damage = 20, type = BulletType.LASER } },
                    primaryWeaponLevel3 = new WeaponEntity() { timeGap = 0.1f, bullet = new BulletEntity() { damage = 5, speed = 1000, type = BulletType.FORWARD } },
                    secondaryWeapon = new WeaponEntity() { timeGap = 0.5f, bullet = new BulletEntity() {damage = 25, speed = 1000, type = BulletType.FOLLOW}},
                    isUnlocked = false,
                    index = 2,
                    levelPrices = new List<int>() { 1000, 3500, 6500 },
                    price = 7500
                };
            }

            public SpaceShipEntity GenerateDefaultSpaceShip4(string playerId)
            {
                return new SpaceShipEntity()
                {
                    playerId = playerId,
                    id = TimeBasedIdGenerator.GenerateId(),
                    level = 1,
                    hp = 450,
                    healthRegen = 5,
                    shield = 50,
                    minSpeed = 20,
                    maxSpeed = 50,
                    sprintSpeed = 150,
                    primaryWeaponLevel1 = new WeaponEntity() { timeGap = 0.1f, bullet = new BulletEntity() { damage = 10, speed = 1000, type = BulletType.FORWARD } },
                    primaryWeaponLevel2 = new WeaponEntity() { timeGap = 0.1f, bullet = new BulletEntity() { damage = 10, speed = 1000, type = BulletType.FORWARD } },
                    primaryWeaponLevel3 = new WeaponEntity() { bullet = new BulletEntity() { damage = 30, type = BulletType.LASER } },
                    secondaryWeapon = new WeaponEntity() { timeGap = 0.5f, bullet = new BulletEntity() {damage = 30, speed = 1000, type = BulletType.FOLLOW}},
                    isUnlocked = false,
                    index = 3,
                    levelPrices = new List<int>() { 1000, 3500, 6500 },
                    price = 11500
                };
            }

        }

        
    }
}