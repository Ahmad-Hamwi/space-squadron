﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Space_Squadron.Assets.Data.Entities;

public class PlayerSpawner : MonoBehaviour
{

    public Material[] camos;
    public GameObject[] spaceShips;
    
    public Vector3 spawnPosition;
    public Vector3 scale = Vector3.one;

    public string name = "Player";


    // true for open world and flase for path 
    public bool openWorldOrPath = true;

    void Start()
    {
        SpaceShipEntity spaceShipEntity;
        if(name == "Player" || name == "Player1")
            spaceShipEntity = DataManager.instance.GetPlayerSpaceShip();
        else
            spaceShipEntity = DataManager.instance.GetSecondPlayerSpaceShip();
        GameObject spaceShip = Instantiate(spaceShips[spaceShipEntity.index], spawnPosition, spaceShips[spaceShipEntity.index].transform.rotation); 
        spaceShip.GetComponent<Renderer>().material = camos[spaceShipEntity.camoIndex];
        spaceShip.name = name;

        spaceShip.transform.localScale = scale;

        if(openWorldOrPath)
        {
            spaceShip.AddComponent<OpenWorldPlayerController>();
        }
        else 
        {
            spaceShip.AddComponent<PathPlayerController>();
        }
    }

}
