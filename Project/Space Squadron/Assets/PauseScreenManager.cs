﻿using Space_Squadron.Assets.Data.Constants;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScreenManager : MonoBehaviour
{

    [SerializeField] private GameObject PausePanel;
    [SerializeField] private GameObject SettingsPanel;

    private void Awake()
    {
        MenuEvents.instance.onSettingsApply += TransitionFromSettingsPanelToPausePanel;
    }

    private void OnEnable()
    {
        Time.timeScale = 0;
    }

    private void OnDisable()
    {
        Time.timeScale = 1;
    }

    public void TransitionFromPausePanelToSettingsPanel()
    {
        PausePanel.SetActive(false);
        SettingsPanel.SetActive(true);
    }

    public void TransitionFromSettingsPanelToPausePanel()
    {
        SettingsPanel.SetActive(false);
        PausePanel.SetActive(true);
    }

    public void TransitionBackToMenus()
    {
        SceneManager.LoadScene(0);
    }
}
