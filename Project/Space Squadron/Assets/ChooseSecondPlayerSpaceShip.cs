﻿using Space_Squadron.Assets.Data.Entities;
using Space_Squadron.Assets.Data.Repositories;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChooseSecondPlayerSpaceShip : MonoBehaviour
{
    private StoreRepository repository;

    [SerializeField]
    private TMP_Text selectionText1;
    [SerializeField]
    private TMP_Text selectionText2;
    [SerializeField]
    private TMP_Text selectionText3;
    [SerializeField]
    private TMP_Text selectionText4;

    [SerializeField]
    private Button selectionButton1;
    [SerializeField]
    private Button selectionButton2;
    [SerializeField]
    private Button selectionButton3;
    [SerializeField]
    private Button selectionButton4;

    private List<SpaceShipEntity> spaceships;

    [SerializeField]
    private GameObject PreGameSceneManager;

    [SerializeField]
    private GameObject SpaceShip1;
    [SerializeField]
    private GameObject SpaceShip2;
    [SerializeField]
    private GameObject SpaceShip3;
    [SerializeField]
    private GameObject SpaceShip4;

    [SerializeField]
    private Material CamoMaterial1;
    [SerializeField]
    private Material CamoMaterial2;
    [SerializeField]
    private Material CamoMaterial3;
    [SerializeField]
    private Material CamoMaterial4;
    [SerializeField]
    private Material CamoMaterial5;
    [SerializeField]
    private Material CamoMaterial6;
    [SerializeField]
    private Material CamoMaterial7;

    private void Awake()
    {
        repository = StoreRepositoryImpl.GetInstance();
    }

    private void OnEnable()
    {
        spaceships = (List<SpaceShipEntity>) repository.LoadPlayerSpaceShips();

        foreach (SpaceShipEntity spaceship in spaceships)
        {
            if (spaceship.index == 0)
            {
                if (spaceship.isUnlocked)
                    selectionText1.text = "Select";
                else
                {
                    selectionText1.text = "Locked";
                    selectionButton1.interactable = false;
                }
            }
            else if (spaceship.index == 1)
            {
                if (spaceship.isUnlocked)
                    selectionText2.text = "Select";
                else
                {
                    selectionText2.text = "Locked";
                    selectionButton2.interactable = false;
                }
            }
            else if (spaceship.index == 2)
            {
                if (spaceship.isUnlocked)
                    selectionText3.text = "Select";
                else
                {
                    selectionText3.text = "Locked";
                    selectionButton3.interactable = false;
                }
            }
            else if (spaceship.index == 3)
            {
                if (spaceship.isUnlocked)
                    selectionText4.text = "Select";
                else
                {
                    selectionText4.text = "Locked";
                    selectionButton4.interactable = false;
                }
            }
        }

        ApplyAllSpaceShipCamo();
    }

    public void OnSelection(int spaceShipIndex)
    {
        int camoIndex = spaceships[spaceShipIndex].camoIndex;
        repository.SaveSecondPlayerSelection(spaceShipIndex, camoIndex);

        PreGameSceneManager.GetComponent<PreGameSceneManager>().TransitionToMulti();
        gameObject.SetActive(false);
    }

    private void ApplyAllSpaceShipCamo()
    {
        foreach (SpaceShipEntity spaceShip in spaceships)
        {
            if (!spaceShip.isUnlocked) continue;
            int camoIndex = spaceShip.camoIndex;
            int spaceShipIndex = spaceShip.index;
            ApplyCamo(spaceShipIndex, camoIndex);
        }
    }

    private void ApplyCamo(int spaceShipIndex, int camoIndex)
    {
        switch (camoIndex)
        {
            case 0:
                switch (spaceShipIndex)
                {
                    case 0: SpaceShip1.GetComponent<Renderer>().material = CamoMaterial1; break;
                    case 1: SpaceShip2.GetComponent<Renderer>().material = CamoMaterial1; break;
                    case 2: SpaceShip3.GetComponent<Renderer>().material = CamoMaterial1; break;
                    case 3: SpaceShip4.GetComponent<Renderer>().material = CamoMaterial1; break;
                }
                break;
            case 1:
                switch (spaceShipIndex)
                {
                    case 0: SpaceShip1.GetComponent<Renderer>().material = CamoMaterial2; break;
                    case 1: SpaceShip2.GetComponent<Renderer>().material = CamoMaterial2; break;
                    case 2: SpaceShip3.GetComponent<Renderer>().material = CamoMaterial2; break;
                    case 3: SpaceShip4.GetComponent<Renderer>().material = CamoMaterial2; break;
                }
                break;
            case 2:
                switch (spaceShipIndex)
                {
                    case 0: SpaceShip1.GetComponent<Renderer>().material = CamoMaterial3; break;
                    case 1: SpaceShip2.GetComponent<Renderer>().material = CamoMaterial3; break;
                    case 2: SpaceShip3.GetComponent<Renderer>().material = CamoMaterial3; break;
                    case 3: SpaceShip4.GetComponent<Renderer>().material = CamoMaterial3; break;
                }
                break;
            case 3:
                switch (spaceShipIndex)
                {
                    case 0: SpaceShip1.GetComponent<Renderer>().material = CamoMaterial4; break;
                    case 1: SpaceShip2.GetComponent<Renderer>().material = CamoMaterial4; break;
                    case 2: SpaceShip3.GetComponent<Renderer>().material = CamoMaterial4; break;
                    case 3: SpaceShip4.GetComponent<Renderer>().material = CamoMaterial4; break;
                }
                break;
            case 4:
                switch (spaceShipIndex)
                {
                    case 0: SpaceShip1.GetComponent<Renderer>().material = CamoMaterial5; break;
                    case 1: SpaceShip2.GetComponent<Renderer>().material = CamoMaterial5; break;
                    case 2: SpaceShip3.GetComponent<Renderer>().material = CamoMaterial5; break;
                    case 3: SpaceShip4.GetComponent<Renderer>().material = CamoMaterial5; break;
                }
                break;
            case 5:
                switch (spaceShipIndex)
                {
                    case 0: SpaceShip1.GetComponent<Renderer>().material = CamoMaterial6; break;
                    case 1: SpaceShip2.GetComponent<Renderer>().material = CamoMaterial6; break;
                    case 2: SpaceShip3.GetComponent<Renderer>().material = CamoMaterial6; break;
                    case 3: SpaceShip4.GetComponent<Renderer>().material = CamoMaterial6; break;
                }
                break;
            case 6:
                switch (spaceShipIndex)
                {
                    case 0: SpaceShip1.GetComponent<Renderer>().material = CamoMaterial7; break;
                    case 1: SpaceShip2.GetComponent<Renderer>().material = CamoMaterial7; break;
                    case 2: SpaceShip3.GetComponent<Renderer>().material = CamoMaterial7; break;
                    case 3: SpaceShip4.GetComponent<Renderer>().material = CamoMaterial7; break;
                }
                break;

        }
    }
}
