﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.ViewData
{
    public enum PlayerMode
    {
        SINGLE_PLAYER = 0, MULTIPLAYER = 1
    }
}
