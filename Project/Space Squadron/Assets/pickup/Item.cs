﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : MonoBehaviour
{
    public abstract string Name { get; }
    public abstract void onPick(GameObject gameObject);

    [SerializeField] protected float pullDistance;

    //Array of all the players in the scene
    private GameObject []players;

    //if the item is being pulled to a player
    private bool attatched = false;

    //the player the item is being pulled towards
    private GameObject goToPlayer;

    private void OnEnable()
    {
        //Find all the players (tagged Player)
        players = GameObject.FindGameObjectsWithTag("Player");
        this.pullDistance = 200f;
    }

    private void LateUpdate()
    {
        if (!attatched) //if it is not being pulled
        {
            foreach (var player in this.players)//go through players
            {
                float dist = Vector3.Distance(player.transform.position
                                                , this.transform.position);

                if (dist <= this.pullDistance) // find the good distance to go to
                {
                    attatched = true;   //attatch the item to a player.
                    goToPlayer = player;    //set the player
                }
            }
        }
        if(goToPlayer != null)  //if the player is set go to it.
        {
            this.transform.position = Vector3.Lerp(this.transform.position, 
                goToPlayer.transform.position, 0.1f);
        }
    }
}
