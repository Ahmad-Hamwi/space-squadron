﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class ScoreItem : Item
{

    [SerializeField] private int ScoreIncrement = 1000;


    public override string Name => "ScoreItem";

    //TODO : Take value from Repo


    public override void onPick(GameObject other)
    {
        //know who picked it
        string picker = other.name;


        //Notify Score to change
        //other.GetComponent<PlayerHandler>().Heal(healthIncrement);
        GameEvents.instance.ScoreChangeForPlayer(ScoreIncrement, picker);

        //destroy that game object
        GameObject.Destroy(gameObject);

    }


    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Hit SomeThing");
        if (other.gameObject.tag == "Player")
        {
            this.onPick(other.gameObject);
        }
    }


}
