﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class ShieldItem: Item
{

    [SerializeField] private float ShieldValue = 30f;


    public override string Name => "ShieldItem";

    //TODO : Take value from Repo


    public override void onPick(GameObject other)
    {
        
        //destroy that game object
        GameObject.Destroy(gameObject);

    }


    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Hit SomeThing");
        if (other.gameObject.tag == "Player")
        {
            this.onPick(other.gameObject);
        }
    }


}
