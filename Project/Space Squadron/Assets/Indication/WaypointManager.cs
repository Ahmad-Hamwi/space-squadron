﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class WaypointManager : MonoBehaviour
{

    //The prefab we use as a waypoint Indicator.
    public RectTransform waypointPrefab;

    //An array of targets that we want to indicate.
    private GameObject[] targets;
    //An array of indicators
    private RectTransform[] indicators;
    /*
        Indicators shall have the same number as the targets.
     */


    //The player Position
    private Transform player;

    //Valuse of the indicator
    private float minX, minY, maxX, maxY;


    private bool found = false;
    private void Start()
    {
    }

    private void Setup()
    {
        //Find all the targets and get their number
        targets = GameObject.FindGameObjectsWithTag("Stations");

        //Find the canvas you want to spwan the prefab into.
        var canvas = GameObject.Find("PlayerCanvas").transform;
        


        indicators = new RectTransform[targets.Length];

        Debug.Log(targets.Length);
        if(targets.Length > 0)
        {
            found = true;
        }

        //Create an indicator for each target.
        for (int i = 0; i < indicators.Length; i++)
        {
            indicators[i] = Instantiate(waypointPrefab, canvas);
        }

        //Find the player.
        player = GameObject.Find("Player").transform;
    }

    private void setXandYvalues()
    {
        /*
            These values are used to keep the indicator on the screen where we get the min X and Y values
            of the indicator that it can remain visible on the screen.
         */

        minX = indicators[0].gameObject.GetComponentInChildren<Image>().GetPixelAdjustedRect().width / 2;
        maxX = Screen.width - minX;

        minY = indicators[0].gameObject.GetComponentInChildren<Image>().GetPixelAdjustedRect().height / 2;
        maxY = Screen.height - minY;
    }

    private void FixedUpdate()
    {

        if (!found)
        {

            Setup();
            //For each single indicator.
            for (int i = 0; i < indicators.Length; i++)
            {
                //set min and max values each frame because if the screen resolution changes it also changes.
                setXandYvalues();

                //Get the equivalent value for the target position
                var screenPos = Camera.main.WorldToScreenPoint(targets[i].transform.position);

                //clamp the values so the indicator doesn't go off the screen.
                screenPos.x = Mathf.Clamp(screenPos.x, minX, maxX);
                screenPos.y = Mathf.Clamp(screenPos.y, minY, maxY);


                //update the text on it to be the distance between the target and the player.
                //indicators[i].gameObject.GetComponentInChildren<Text>().text = Vector3.Distance(player.position, targets[i].transform.position).ToString("0")  + " m";

                //if it is behind hide it
                if (Vector3.Dot((targets[i].transform.position - player.position), player.forward) < 0)
                {

                    //if the target is on the left of the screen flip it because we are looking backwards
                    if (screenPos.x < Screen.width / 2)
                    {
                        screenPos.x = maxX;
                    }
                    else
                    {
                        screenPos.x = minX;
                    }

                    if (screenPos.y < Screen.height / 2)
                    {
                        screenPos.y = maxY;
                    }
                    else
                    {
                        screenPos.y = minY;
                    }

                }


                //set its position to be the same as the target position.
                indicators[i].position = screenPos;


            }
        }

        
    }

}
