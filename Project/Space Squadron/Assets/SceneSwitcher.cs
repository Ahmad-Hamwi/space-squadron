﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    int multiDeaths;
    // Start is called before the first frame update
    void Start()
    {
        GameEvents.instance.onLevelWin += LoadNextLevel;
        GameEvents.instance.onLevelLose += Instance_onLevelLose;

        GameEvents.instance.onPlayerDeath += HandlePlayerDeath;
        multiDeaths = 0;

        //GameEvents.instance.onReloadCurrentScene += ReloadCurrentScene;
    }



    private void HandlePlayerDeath(string name)
    {
        if(name == "Player")
        {
            //show lose menu
            Debug.Log("Show Lost Menu");
            GameObject.Find("EndLevelManager").GetComponent<EndLevelManager>().ShowLoseMenu();
        }
        else
        {
            multiDeaths++;


            Debug.Log(multiDeaths); 
            
            if (multiDeaths == 2)
            {
                //end game
                GameObject.Find("EndLevelManager").GetComponent<EndLevelManager>().ShowLoseMenu();
            }
            /*else //it is less than 2
            {
                //dont do anything and wait for other player to die.
            }*/
        }
    }

    private void Instance_onLevelLose()
    {
        //reload level
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


    public void LoadNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void ReloadCurrentScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("PreGame");
    }
}
