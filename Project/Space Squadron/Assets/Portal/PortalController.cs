﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PortalController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("portal controller is running!");
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        /*if(other.gameObject.name == "Player")
        {
            GameObject.Find("CoinsManager").GetComponent<CoinsManager>().calculateCoins();
            SceneManager.LoadScene(1);
            //GameEvents.instance.LevelWin();
        }*/
        if(other.gameObject.tag == "Player")
            GameObject.Find("EndLevelManager").GetComponent<EndLevelManager>().ShowAdvanceMenu();
    }
}
