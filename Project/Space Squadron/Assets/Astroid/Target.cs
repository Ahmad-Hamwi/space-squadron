﻿using UnityEngine;
using UnityEngine.UI;

public abstract class Target : MonoBehaviour
{
    protected HealthSystem healthSystem;
    protected HealthBar healthBar;
    protected float healthRegen;
    protected float shield;
    protected int scoreValue;
    protected float health;

    protected string playerShooting;

    //attributes for the display of the healthbar.
    bool display = false;
    public float healthBarLifeTime = 1.5f;
    Transform player;
    float lifeTimer = 1f;




    protected void HealthSystem_onHealthChanged()
    {

        //find the player
        //player = GameObject.Find(playerShooting).transform;
        // Debug.Log("HealthWas Changed!");

        //update the value of the healthBarObject
        this.healthBar.healthBarObject.value = this.healthSystem.GetHealthPercentage();

        //The Lerp of the color in the healthBarObject
        this.healthBar.healthBarObject.gameObject.transform.Find("Fill Area").Find("Fill").GetComponent<Image>().color =
                Color.Lerp(this.healthBar.MinHealthColor, this.healthBar.MaxHealthColor, this.healthSystem.GetHealthPercentage());

        //Allow displaying it and set the timer to be the default timer.
        if (player != null)
        {
            display = true;
        }
        lifeTimer = this.healthBarLifeTime;
    }

    private void Instance_onTargetShooting(string playerName)
    {
        this.playerShooting = playerName;
        player = GameObject.Find(playerShooting).transform;
    }

    private void FixedUpdate()
    {
        if (display) //display is true when this object health is changed.
        {
            //take the current object position and flip it to the screen coords.
            var pos = Camera.main.WorldToScreenPoint(this.transform.position);


            //show the health bar.
            this.healthBar.healthBarObject.gameObject.transform.parent.gameObject.SetActive(true);


            //if it is behind you dont show its healthbar.
            if (Vector3.Dot((this.transform.position - player.position).normalized, player.forward) < 0)
            {
                this.healthBar.healthBarObject.gameObject.transform.parent.gameObject.SetActive(false);
            }
            this.healthBar.healthBarObject.transform.position = pos;
            //count down and when you are done hide the health bar and dont allow displaying it until notified.
            lifeTimer -= Time.deltaTime;
            if (lifeTimer <= 0)
            {
                this.healthBar.healthBarObject.gameObject.transform.parent.gameObject.SetActive(false);
                display = false;
            }


        }
    }

    bool isDie = false;

    public void Damage(float dmg)
    {
        GameEvents.instance.onTargetShooting += Instance_onTargetShooting;

        //Debug.Log("PLayer is  " + playerShooting);

        //deal damage.
        if (healthSystem.GetHealth() > 0)
        {
            this.healthSystem.TakeDamage(dmg);
        }

        //check for death.
        if(this.healthSystem.GetHealth() <= 0)
        {
            if(!isDie)
            {
                isDie = true;
                this.Die();
            }
        }
    }

    public abstract void Die();

}
